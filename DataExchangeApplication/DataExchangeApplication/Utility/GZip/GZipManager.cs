﻿// -----------------------------------------------------------------------
// <copyright file="GZipManager.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/25/2016 1:16:02 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.Utility.GZip
{
    using System.IO;
    using System.IO.Compression;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class GZipManager
    {
        /// <summary>
        /// Decompress File and Read Context
        /// </summary>
        /// <param name="fileName">File Path</param>
        /// <returns>File Content</returns>
        public static string DecompressStringFromFile(string fileName)
        {
            string temp = Path.GetTempFileName();

            byte[] gzip = File.ReadAllBytes(fileName);

            // Create a GZIP stream with decompression mode.
            // ... Then create a buffer and write into while reading from the GZIP stream.
            using (GZipStream stream = new GZipStream(new MemoryStream(gzip), CompressionMode.Decompress))
            {
                const int BufferSize = 4096;
                byte[] buffer = new byte[BufferSize];
                using (MemoryStream memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, BufferSize);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    }
                    while (count > 0);
                    byte[] fileData = memory.ToArray();
                    File.WriteAllBytes(temp, fileData);
                }
            }

            return File.ReadAllText(temp);
        }

        /// <summary>
        /// Compress string to file
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="value">File Content</param>
        public static void CompressStringToFile(string fileName, string value)
        {
            // A.
            // Write string to temporary file.
            string temp = Path.GetTempFileName();
            File.WriteAllText(temp, value);

            // B.
            // Read file into byte array buffer.
            byte[] b;
            using (FileStream f = new FileStream(temp, FileMode.Open))
            {
                b = new byte[f.Length];
                f.Read(b, 0, (int)f.Length);
            }

            // C.
            // Use GZipStream to write compressed bytes to target file.
            using (FileStream f2 = new FileStream(fileName, FileMode.Create))
            using (GZipStream gz = new GZipStream(f2, CompressionMode.Compress, false))
            {
                gz.Write(b, 0, b.Length);
            }
        }
    }
}
