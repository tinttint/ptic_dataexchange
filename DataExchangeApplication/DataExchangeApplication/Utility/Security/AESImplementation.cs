﻿// -----------------------------------------------------------------------
// <copyright file="AESImplementation.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/25/2016 10:59:52 AM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.Utility.Security
{
    using System.IO;

    /// <summary>
    /// AES Implementation
    /// </summary>
    public class AESImplementation
    {
        /// <summary>
        /// create an object of AES Implementation
        /// </summary>
        private static AESImplementation instance = new AESImplementation();

        /// <summary>
        /// create an object for Step One Engine
        /// </summary>
        private AdvancedEncryptionStandard.SimpleAES stepOneEncrypt;

        /// <summary>
        /// create an object for Step Two Engine
        /// </summary>
        private AdvancedEncryptionStandard.SimpleAES stepTwoEncrypt;

        /// <summary>
        /// Prevents a default instance of the AESImplementation class from being created.
        /// </summary>
        private AESImplementation()
        {
            byte[] stepOneKey = File.ReadAllBytes("Key&Vector\\StepOne.ptickey");
            byte[] stepOneVector = File.ReadAllBytes("Key&Vector\\StepOne.pticvector");
            byte[] stepTwoKey = File.ReadAllBytes("Key&Vector\\StepTwo.ptickey");
            byte[] stepTwoVector = File.ReadAllBytes("Key&Vector\\StepTwo.pticvector");

            this.stepOneEncrypt = new AdvancedEncryptionStandard.SimpleAES(stepOneKey, stepOneVector);
            this.stepTwoEncrypt = new AdvancedEncryptionStandard.SimpleAES(stepTwoKey, stepTwoVector);
        }

        /// <summary>
        /// AESImplementation Singleton Object
        /// </summary>
        /// <returns>Singleton Object</returns>
        public static AESImplementation GetInstance()
        {
            return instance;
        }

        /// <summary>
        /// Encrypt Plain Text 
        /// By using Two Step
        /// </summary>
        /// <param name="plainText">Plain Text</param>
        /// <returns>Encrypted Text</returns>
        public string Encrypt(string plainText)
        {
            string encryptedString = string.Empty;
            string stepOneEncrypted = this.stepOneEncrypt.EncryptToString(plainText);
            ////encryptedString = this.stepOneEncrypt.EncryptToString(stepOneEncrypted);
            return stepOneEncrypted;
        }

        /// <summary>
        /// Decrypt Cypher Two Times
        /// </summary>
        /// <param name="cypherText">Cypher Text</param>
        /// <returns>Plain Text</returns>
        public string Decrypt(string cypherText)
        {
            string plainText = string.Empty;
            string stepTwoPlainText = this.stepOneEncrypt.DecryptString(cypherText);
            ////plainText = this.stepOneEncrypt.EncryptToString(stepTwoPlainText);
            return stepTwoPlainText;
        }
    }
}
