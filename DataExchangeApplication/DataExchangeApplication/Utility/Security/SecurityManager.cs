﻿// -----------------------------------------------------------------------
// <copyright file="SecurityManager.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/25/2016 10:58:22 AM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.Utility.Security
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SecurityManager
    {
        /// <summary>
        /// create object of SecurityManager
        /// </summary>
        private static SecurityManager instance = new SecurityManager();

        /// <summary>
        /// Prevents a default instance of the SecurityManager class from being created.
        /// </summary>
        private SecurityManager()
        {
        }

        /// <summary>
        /// SecurityManager Singleton Object
        /// </summary>
        /// <returns>Singleton Object</returns>
        public static SecurityManager GetInstance()
        {
            return instance;
        }

        /// <summary>
        /// Encrypt Incoming JSON String
        /// </summary>
        /// <param name="jsonString">JSON string</param>
        /// <returns>Encrypted Byte String</returns>
        public string EncryptJSON(string jsonString)
        {
            AESImplementation cryptoEngine = AESImplementation.GetInstance();
            return cryptoEngine.Encrypt(jsonString);
        }

        /// <summary>
        /// Decrypt Cipher Byte string to JSON
        /// </summary>
        /// <param name="cipherString">Cipher Byte String</param>
        /// <returns>JSON string</returns>
        public string DecryptCipher(string cipherString)
        {
            AESImplementation cryptoEngine = AESImplementation.GetInstance();
            return cryptoEngine.Decrypt(cipherString);
        }
    }
}
