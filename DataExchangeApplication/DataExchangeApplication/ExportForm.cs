﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DataExchangeApplication
{
    public partial class ExportForm : Form
    {
        public delegate void CrossThreadInvoke(object obj);
        public delegate void CrossThreadInvoke2(object obj,object obj2);

        public ExportForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ExportingData() 
        {
            try
            {
                if (Properties.Settings.Default.CurrentLocation == Utility.ConstantStringCollection.HEADOFFICE)
                {
                    SetProgress(6);

                    Service.DataExportService serviceExporter = new Service.DataExportService();
                    serviceExporter.TaskCompleteProgress += new Service.DataExportService.CompleteProgess(serviceExporter_TaskCompleteProgress);

                    string finalExport = serviceExporter.ExportHeadOfficeData();

                    //string fileName = string.Format("{0}\\{1}_{2}.gz", Properties.Settings.Default.ExportFilePath, Utility.ConstantStringCollection.HEADOFFICE, DateTime.Now.ToString("yyyyMMdd"));
                    string searchPattern = string.Format("{1}_{2}.gz", Properties.Settings.Default.ExportFilePath, Utility.ConstantStringCollection.HEADOFFICE, DateTime.Now.ToString("yyyyMMdd") + "*");

                    string[] fileNameList = Directory.GetFiles(Properties.Settings.Default.ExportFilePath, searchPattern);
                    string fileName = string.Format("{0}\\{1}_{2}.gz", Properties.Settings.Default.ExportFilePath, Utility.ConstantStringCollection.HEADOFFICE, DateTime.Now.ToString("yyyyMMdd") + "_[" + (fileNameList.Length + 1) + "]");

                    SetMessage("Exporting Data to File!");
                    IncreaseProgress(6);
                    Utility.GZip.GZipManager.CompressStringToFile(fileName, finalExport);


                    DataExchangeApplication.Service.DataExportService.UpdateHOEntity();

                    MessageBox.Show(Utility.ConstantStringCollection.EXPORTSUCCESS + "\n" + fileName);
                }
                else
                {

                    SetProgress(2);

                    Service.DataExportService serviceExporter = new Service.DataExportService();
                    serviceExporter.TaskCompleteProgress += new Service.DataExportService.CompleteProgess(serviceExporter_TaskCompleteProgress);

                    string finalExport = serviceExporter.ExportFactoryData();

                    DataExchangeApplication.Service.DataExportService.UpdateFactoryEntity();

                    string searchPattern = string.Format("{1}_{2}.gz", Properties.Settings.Default.ExportFilePath, Utility.ConstantStringCollection.FACTORY, DateTime.Now.ToString("yyyyMMdd") + "*");

                    string[] fileNameList = Directory.GetFiles(Properties.Settings.Default.ExportFilePath, searchPattern);
                    string fileName = string.Format("{0}\\{1}_{2}.gz", Properties.Settings.Default.ExportFilePath, Utility.ConstantStringCollection.FACTORY, DateTime.Now.ToString("yyyyMMdd") + "_[" + (fileNameList.Length + 1) + "]");


                    //string fileName = string.Format("{0}\\{1}_{2}.gz", Properties.Settings.Default.ExportFilePath, Utility.ConstantStringCollection.FACTORY, DateTime.Now.ToString("yyyyMMdd"));

                    Utility.GZip.GZipManager.CompressStringToFile(fileName, finalExport);

                    MessageBox.Show(Utility.ConstantStringCollection.EXPORTSUCCESS + "\n" + fileName);
                }
            }
            catch (Exception err) 
            {
                MessageBox.Show(err.Message, "Exception");
            }
        }

        private void ExportForm_Load(object sender, EventArgs e)
        {
            System.Threading.Thread exporterThread = new System.Threading.Thread(ExportingData);
            exporterThread.Start();
            dgvLog.Rows.Clear();
        }

        void serviceExporter_TaskCompleteProgress(object sender, object e)
        {
            try
            {
                IncreaseProgress(this.pgTotalTask.Value + 1);
                SetMessage(sender);
                AddToLog(sender, e);
            }
            catch (Exception err) 
            {
                MessageBox.Show(err.Message);
            }
        }

        private void IncreaseProgress(object obj) 
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new CrossThreadInvoke(IncreaseProgress), obj);
            }
            else 
            {
                this.pgTotalTask.Value = (int)obj;
            }

        }

        private void SetProgress(object obj)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new CrossThreadInvoke(SetProgress), obj);
            }
            else
            {
                this.pgTotalTask.Maximum = (int)obj;
                this.pgTotalTask.Value = 0;
            }
        }

        private void SetMessage(object obj) 
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new CrossThreadInvoke(SetMessage), obj);
            }
            else
            {
                this.lblMessage.Text = (string)obj;
            }
        }

        private void AddToLog(object obj,object obj2)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new CrossThreadInvoke2(AddToLog), obj,obj2);
            }
            else
            {
                dgvLog.Rows.Add();
                dgvLog.Rows[dgvLog.Rows.Count - 1].Cells[0].Value = obj;
                dgvLog.Rows[dgvLog.Rows.Count - 1].Cells[1].Value = obj2;
            }
        }
    }
}
