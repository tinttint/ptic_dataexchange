﻿// -----------------------------------------------------------------------
// <copyright file="DataImportService.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 04/25/2016 2:21:22 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.Service
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.BLL;
    using DataExchangeApplication.DAL;
    using DataExchangeApplication.DAO;
    using Newtonsoft.Json;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class DataImportService
    {


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void CompleteProgess(object sender, object e);


        /// <summary>
        /// 
        /// </summary>
        public event CompleteProgess TaskCompleteProgress;

        /// <summary>
        /// Import the exported string
        /// </summary>
        /// <param name="encryptedJSON">encryptedJSON string</param>
        /// <returns>database operation status</returns>
        public string ImportData(string encryptedJSON)
        {
            Utility.Security.SecurityManager manager = Utility.Security.SecurityManager.GetInstance();
            if (Properties.Settings.Default.CurrentLocation == Utility.ConstantStringCollection.HEADOFFICE)
            {
                string dataExchangeJSON = manager.DecryptCipher(encryptedJSON);
                DataExchangeTemplateRoot root = JsonConvert.DeserializeObject(dataExchangeJSON, typeof(DataExchangeTemplateRoot)) as DataExchangeTemplateRoot;
                foreach (DataExchangeItemTemplate item in root.ItemList)
                {
                    if (item.DataName == Utility.ConstantStringCollection.FINISHEDGOODSISSUE)
                    {
                        string finishedGDJson = manager.DecryptCipher(item.EncryptedJSON);
                        FGIssueNoteRootDAO finishedRoot = JsonConvert.DeserializeObject(finishedGDJson, typeof(FGIssueNoteRootDAO)) as FGIssueNoteRootDAO;
                        string importedString = new DataExchangeApplication.BLL.FinishedGoodsRequestImportBL().ImportFactoryData(finishedRoot.FinishedGoodsISCollection);
                        if (TaskCompleteProgress != null)
                        {
                            string msg = Utility.ConstantStringCollection.FINISHEDGOODSREQUEST;
                            TaskCompleteProgress(msg, finishedRoot.FinishedGoodsISCollection.Count);
                        }
                    }
                    else if (item.DataName == Utility.ConstantStringCollection.SERVICEDN)
                    {
                        string finishedGDJson = manager.DecryptCipher(item.EncryptedJSON);
                        ServieBatteryDeliveryNoteRootDAO finishedRoot = JsonConvert.DeserializeObject(finishedGDJson, typeof(ServieBatteryDeliveryNoteRootDAO)) as ServieBatteryDeliveryNoteRootDAO;
                        string importedString = new DataExchangeApplication.BLL.ServiceBatteryDNImportBL().ImportData(finishedRoot.DeliveryNoteCollections);
                        if (TaskCompleteProgress != null)
                        {
                            string msg = Utility.ConstantStringCollection.SERVICEDN;
                            TaskCompleteProgress(msg, finishedRoot.DeliveryNoteCollections.Count);
                        }
                    
                    }
                }
                TaskCompleteProgress("Rename File!",0);
                
                return Utility.ConstantStringCollection.IMPORTSUCCESS;
            }
            else
            {
                string dataExchangeJSON = manager.DecryptCipher(encryptedJSON);

                DataExchangeTemplateRoot root = JsonConvert.DeserializeObject(dataExchangeJSON, typeof(DataExchangeTemplateRoot)) as DataExchangeTemplateRoot;
                foreach (DataExchangeItemTemplate item in root.ItemList)
                {
                    if (item.DataName == Utility.ConstantStringCollection.FINISHEDGOODSREQUEST)
                    {
                        string finishGDReqJSON = manager.DecryptCipher(item.EncryptedJSON);
                        FinishedGoodsRootDAO finishedGoodsRoot = JsonConvert.DeserializeObject(finishGDReqJSON, typeof(FinishedGoodsRootDAO)) as FinishedGoodsRootDAO;
                        string importedString = new DataExchangeApplication.BLL.FinishedGoodsRequestImportBL().ImportData(finishedGoodsRoot.FinishedGoodsRequisitionCollection);
                        if (TaskCompleteProgress != null)
                        {
                            string msg = Utility.ConstantStringCollection.FINISHEDGOODSREQUEST;
                            TaskCompleteProgress(msg, finishedGoodsRoot.FinishedGoodsRequisitionCollection.Count);
                        }
                    }
                    else if (item.DataName == Utility.ConstantStringCollection.SALESPLAN)
                    {
                        string salePlanJSON = manager.DecryptCipher(item.EncryptedJSON);
                        SalesPlanRootDAO salePlanRoot = JsonConvert.DeserializeObject(salePlanJSON, typeof(SalesPlanRootDAO)) as SalesPlanRootDAO;
                        string importedString = new DataExchangeApplication.BLL.SalesPlanImportBL().ImportData(salePlanRoot.SalesPlanRequisitionCollection);
                        if (TaskCompleteProgress != null)
                        {
                            string msg = Utility.ConstantStringCollection.SALESPLAN;
                            TaskCompleteProgress(msg, salePlanRoot.SalesPlanRequisitionCollection.Count);
                        }
                    }
                    else if (item.DataName == Utility.ConstantStringCollection.SHOWROOMMOVEMENT) 
                    {
                        string showRoomMovement = manager.DecryptCipher(item.EncryptedJSON);
                        ShowRoomMovementRootDAO showRoom = JsonConvert.DeserializeObject(showRoomMovement, typeof(ShowRoomMovementRootDAO)) as ShowRoomMovementRootDAO;
                        string importedString = new DataExchangeApplication.BLL.ShowRoomMovementImportBL().ImportData(showRoom.ShowRoomMovementCollection);
                        if (TaskCompleteProgress != null)
                        {
                            string msg = Utility.ConstantStringCollection.SHOWROOMMOVEMENT;
                            TaskCompleteProgress(msg, showRoom.ShowRoomMovementCollection.Count);
                        }
                    }
                    else if (item.DataName == Utility.ConstantStringCollection.SERVICELEDGER) 
                    {
                        string showRoomMovement = manager.DecryptCipher(item.EncryptedJSON);
                        ServiceLedgerRootDAO svcLedgerRoot = JsonConvert.DeserializeObject(showRoomMovement, typeof(ServiceLedgerRootDAO)) as ServiceLedgerRootDAO;
                        string importedString = new DataExchangeApplication.BLL.ServiceBatteryLedgerImportBL().ImportData(svcLedgerRoot.ServiceLedgerCollection);
                        if (TaskCompleteProgress != null)
                        {
                            string msg = Utility.ConstantStringCollection.SERVICELEDGER;
                            TaskCompleteProgress(msg, svcLedgerRoot.ServiceLedgerCollection.Count);
                        }
                    }
                    else if (item.DataName == Utility.ConstantStringCollection.SERVICEDN) 
                    {
                        string showRoomMovement = manager.DecryptCipher(item.EncryptedJSON);
                        ServieBatteryDeliveryNoteRootDAO svcDNRoot = JsonConvert.DeserializeObject(showRoomMovement, typeof(ServieBatteryDeliveryNoteRootDAO)) as ServieBatteryDeliveryNoteRootDAO;
                        string importedString = new DataExchangeApplication.BLL.ServiceBatteryDNImportBL().ImportData(svcDNRoot.DeliveryNoteCollections);
                        if (TaskCompleteProgress != null)
                        {
                            string msg = Utility.ConstantStringCollection.SERVICEDN;
                            TaskCompleteProgress(msg, svcDNRoot.DeliveryNoteCollections.Count);
                        }
                    }
                }
                TaskCompleteProgress("Rename File!", 0);
                return Utility.ConstantStringCollection.IMPORTSUCCESS;
            }
        }
    }
}
