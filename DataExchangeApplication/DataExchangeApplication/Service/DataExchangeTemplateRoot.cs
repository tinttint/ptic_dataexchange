﻿    // -----------------------------------------------------------------------
// <copyright file="DataExchangeTemplateRoot.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/25/2016 12:23:06 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.Service
{
    /// <summary>
    /// Template Root of Data Exchange
    /// </summary>
    public class DataExchangeTemplateRoot
    {
        /// <summary>
        /// Gets or sets Created Location
        /// </summary>
        public string CreatedLocation { get; set; }

        /// <summary>
        /// Gets or sets Created Date
        /// </summary>
        public System.DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets Item List
        /// </summary>
        public System.Collections.Generic.List<DataExchangeItemTemplate> ItemList { get; set; }
    }
}
