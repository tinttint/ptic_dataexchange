﻿// -----------------------------------------------------------------------
// <copyright file="DataExchangeItemTemplate.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/25/2016 12:27:39 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.Service
{
    /// <summary>
    /// Data Exchange Item
    /// </summary>
    public class DataExchangeItemTemplate
    {
        /// <summary>
        /// Gets or sets Data Name
        /// </summary>
        public string DataName { get; set; }

        /// <summary>
        /// Gets or sets Encrypted JSON String
        /// </summary>
        public string EncryptedJSON { get; set; }
    }
}
