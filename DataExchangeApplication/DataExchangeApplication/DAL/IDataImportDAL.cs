﻿// -----------------------------------------------------------------------
// <copyright file="IDataImportDAL.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/22/2016 11:14:04 AM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAL
{
    /// <summary>
    /// This is the interface for all data Import
    /// </summary>
    public interface IDataImportDAL
    {
        /// <summary>
        /// Importing Data 
        /// </summary>
        /// <param name="data">Data to import</param>
        /// <returns>operation status</returns>
       string ImportData(object data);
    }
}
