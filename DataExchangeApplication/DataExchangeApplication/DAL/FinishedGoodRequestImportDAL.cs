﻿// -----------------------------------------------------------------------
// <copyright file="FinishedGoodRequestImportDAL.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/22/2016 11:16:58 AM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------
namespace DataExchangeApplication.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataExchangeApplication.DAO;
    using Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class FinishedGoodRequestImportDAL : IDataImportDAL
    {
        /// <summary>
        /// Import Finished Goods Request From Head Office
        /// </summary>
        /// <param name="data">Finished Goods Data From Head Office</param>
        /// <returns>database operation status</returns>
        public string ImportData(object data)
        {
            DataManager manager = DataManager.GetInstance();
            if (Properties.Settings.Default.CurrentLocation == Utility.ConstantStringCollection.HEADOFFICE)
            {
                List<FGIssueNoteInfoDAO> issueInfo = data as List<FGIssueNoteInfoDAO>;

                    foreach (FGIssueNoteInfoDAO issue in issueInfo)
                    {
                        Entity.FGIssueNoteInfo info = new FGIssueNoteInfo();
                        Entity.FGRequest request = (from fg in manager.HeadOfficeContext.FGRequests where fg.IsDeleted == false && fg.ReqVouNo == issue.HOReqNo select fg).FirstOrDefault();

                        if (request != null)
                        {
                            request.IssueDate = issue.IssueDate;
                            request.IssueVouNo = issue.IssueNoteNo;
                            request.FactoryFormRemark = issue.HORemark;
                            request.LastModified = DateTime.Now;

                            foreach (FGRequestDetail detail in request.FGRequestDetails) 
                            {
                                FGIssueNoteDetailDAO issueDtl = (from issueDetails in issue.Details where issueDetails.ProdID == detail.ProductID select issueDetails).FirstOrDefault();
                                if (issueDtl != null)
                                {
                                    detail.IssueQty = issueDtl.Qty;
                                    detail.FactoryRemark = issueDtl.HORemark;
                                    detail.LastModified = DateTime.Now;
                                }

                                ////For StockInWarehouse
                                Entity.StockInWarehouse stockInWarehouse = (from siw in manager.HeadOfficeContext.StockInWarehouses where siw.ProductID == detail.ProductID && siw.WarehouseID == 2 select siw).FirstOrDefault();
                                if (stockInWarehouse != null)
                                {
                                    stockInWarehouse.Qty += detail.Qty.Value;
                                }
                                else
                                {
                                    Entity.StockInWarehouse siw = new StockInWarehouse();
                                    siw.ProductID = detail.ProductID.Value;
                                    siw.WarehouseID = 2;
                                    siw.Qty = detail.Qty.Value;
                                    siw.DateAdded = DateTime.Now;
                                    siw.LastModified = DateTime.Now;
                                    siw.IsDeleted = false;
                                    manager.HeadOfficeContext.StockInWarehouses.AddObject(siw);
                                }

                                /// For In/Out Transaction
                                manager.HeadOfficeContext.WarehouseInOuts.AddObject(new WarehouseInOut()
                                {
                                    WarehouseID = 1,
                                    ProductID = detail.ProductID.Value,
                                    Qty = detail.IssueQty.Value*-1,
                                    Date = issue.IssueDate.Date,
                                    StockBy = 1,
                                    Remark = "Data Transfer",
                                    DateAdded = DateTime.Now.Date,
                                    IsDeleted = false
                                });

                                manager.HeadOfficeContext.WarehouseInOuts.AddObject(new WarehouseInOut()
                                {
                                    WarehouseID = 2,
                                    ProductID = detail.ProductID.Value,
                                    Qty = detail.IssueQty.Value,
                                    Date = issue.IssueDate.Date,
                                    StockBy = 0,
                                    Remark = "Data Transfer",
                                    DateAdded = DateTime.Now.Date,
                                    IsDeleted = false
                                });
                          }
                      }
               }

                manager.HeadOfficeContext.SaveChanges();
               return Utility.ConstantStringCollection.IMPORTSUCCESS;
            }
            else
            {
                List<FinishedGoodsRequisitionDAO> daoList = data as List<DAO.FinishedGoodsRequisitionDAO>;
                foreach (FinishedGoodsRequisitionDAO dao in daoList)
                {
                    Entity.FGRequisitionInfo finishedgoodRequest = new FGRequisitionInfo();

                    finishedgoodRequest.ID = dao.ID;
                    finishedgoodRequest.IsDeleted = dao.IsDeleted;
                    finishedgoodRequest.RequestDeptID = 25;
                    finishedgoodRequest.Date = dao.ReqDate;
                    finishedgoodRequest.DateAdded = DateTime.Now;
                    finishedgoodRequest.Remark = dao.Remark;
                    finishedgoodRequest.IsDeleted = false;
                    finishedgoodRequest.RequestNo = dao.ReqNo;
                    finishedgoodRequest.ReferenceNo = dao.ReqNo;
                    finishedgoodRequest.LastModified = DateTime.Now;
                    finishedgoodRequest.IsUsed = false;
                    finishedgoodRequest.RequestPersonName = dao.RequesterName;
                    finishedgoodRequest.FGRequisitionDetails = new System.Data.Objects.DataClasses.EntityCollection<FGRequisitionDetail>();
                    foreach (FinishedGoodsRequisitionDetailsDAO details in dao.Details)
                    {
                        finishedgoodRequest.FGRequisitionDetails.Add(new FGRequisitionDetail()
                        {
                            ProductID = details.ProductID,
                            Qty = details.RequsetQty,
                            Remark = details.Remark,
                            DateAdded = DateTime.Now,
                            LastModified = DateTime.Now,
                            isDeleted = false
                        });
                    }

                    // Checking Duplicate Data Before Import

                    Entity.FGRequisitionInfo duplicateInfo = (from fgReqInfo in manager.FactoryContext.FGRequisitionInfoes where fgReqInfo.HORequestID == finishedgoodRequest.HORequestID || fgReqInfo.ReferenceNo == finishedgoodRequest.ReferenceNo select fgReqInfo).FirstOrDefault();
                    if (duplicateInfo != null)
                    {
                        //Duplicate Found
                        //throw new Exception("Duplicate Entry! May be Double Import");
                    }
                    else
                    {
                        manager.FactoryContext.FGRequisitionInfoes.AddObject(finishedgoodRequest);
                        manager.FactoryContext.SaveChanges();
                    }
                }

               
                return Utility.ConstantStringCollection.IMPORTSUCCESS;
            }
        }
    }
}
