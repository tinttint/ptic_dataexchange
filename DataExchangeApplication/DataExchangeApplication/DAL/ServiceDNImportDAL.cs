﻿// -----------------------------------------------------------------------
// <copyright file="ServiceDNImportDAL.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.DAO;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ServiceDNImportDAL:IDataImportDAL
    {
        /// <summary>
        /// Import SalesPlan from Head Office
        /// </summary>
        /// <param name="data">SalesPlan data from Head Office</param>
        /// <returns>database operation status</returns>
        public string ImportData(object data)
        {
            List<ServiceDeliveryNoteInfoDAO> saleList = data as List<ServiceDeliveryNoteInfoDAO>;
            DataManager manager = DataManager.GetInstance();

            if (Properties.Settings.Default.CurrentLocation == Utility.ConstantStringCollection.FACTORY)
            {
                foreach (ServiceDeliveryNoteInfoDAO svcDN in saleList)
                {
                    Entity.ServiceRecieveShowRoomInfo info = new Entity.ServiceRecieveShowRoomInfo()
                    {
                        CheckedBy = null,
                        IsDeleted = false,
                        IssuedNo = svcDN.DeliveryNoteNo,
                        RecieveDate = svcDN.IssuedDate,
                        RecievedNo = string.Empty,
                        RecieverId = null,
                        RefNo = string.Empty,
                        Sender = svcDN.Issued,
                        ServiceRecieveShowRoomDetails = svcDN.GetDetailsEntities(manager.FactoryContext),
                        TransportedBy = svcDN.Prepare,
                        VenNo = (svcDN.VenNo == null ? string.Empty : svcDN.VenNo)
                    };

                    //Checking Duplicate
                    Entity.ServiceRecieveShowRoomInfo infoDuplicate = (from duplicateInfo in manager.FactoryContext.ServiceRecieveShowRoomInfoes where duplicateInfo.IssuedNo == info.IssuedNo select duplicateInfo).FirstOrDefault();
                    if (infoDuplicate == null)
                    {
                        manager.FactoryContext.ServiceRecieveShowRoomInfoes.AddObject(info);
                    }
                }

                manager.FactoryContext.SaveChanges();
                return Utility.ConstantStringCollection.IMPORTSUCCESS;
            }
            else 
            {
                foreach (ServiceDeliveryNoteInfoDAO svcDN in saleList)
                {
                    foreach (ServiceDeliveryNoteDetailsDAO dtl in svcDN.ItemList)
                    {
                        Entity.ServiceLedger svcLedger = (from svcL in manager.HeadOfficeContext.ServiceLedgers
                                                          where svcL.JobNo == dtl.JobNo
                                                          select svcL).FirstOrDefault();
                        if (svcLedger != null) 
                        {
                            svcLedger.FactoryRecieveDate = svcDN.IssuedDate;
                            svcLedger.FactoryManagerRemark = dtl.FactoryMangerRemark;
                            svcLedger.F = dtl.Fault;
                        }
                    }
                }

                manager.HeadOfficeContext.SaveChanges();
                return Utility.ConstantStringCollection.IMPORTSUCCESS;
            }
        }
    }
}
