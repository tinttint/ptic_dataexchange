﻿// -----------------------------------------------------------------------
// <copyright file="IDataExportDAL.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/23/2016 1:42:57 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAL
{
    /// <summary>
    /// This is the interface for all data Import
    /// </summary>
    public interface IDataExportDAL
    {
        /// <summary>
        /// Exporting Data 
        /// </summary>
        /// <returns>Export Data List</returns>
        object ExportData();

        /// <summary>
        /// Changing Export Status
        /// </summary>
        void ChangeExportStatus();
    }
}
