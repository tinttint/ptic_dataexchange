﻿// -----------------------------------------------------------------------
// <copyright file="DataManager.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/23/2016 3:45:18 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAL
{
    using Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class DataManager
    {
        #region Fields

        /// <summary>
        /// create an object of Data Manager
        /// </summary>
        private static DataManager instance = new DataManager();

        /// <summary>
        /// create an object of Factory Data Context
        /// </summary>
        private static Proven_FactoryEntities factoryContext = new Proven_FactoryEntities();

        /// <summary>
        /// create an object of Head Office Data Context
        /// </summary>
        private static PTIC_HOEntities headOfficeContext = new PTIC_HOEntities();

        #endregion

        /// <summary>
        /// Prevents a default instance of the DataManager class from being created.
        /// </summary>
        private DataManager()
        {
        }

        #region Properties

        /// <summary>
        /// Gets read only Factory Context
        /// </summary>
        public Proven_FactoryEntities FactoryContext
        {
            get { return factoryContext; }
        }

        /// <summary>
        /// Gets read only Head Office Context
        /// </summary>
        public PTIC_HOEntities HeadOfficeContext
        {
            get { return headOfficeContext; }
        }

        #endregion

        /// <summary>
        /// DataManager Singleton Object
        /// </summary>
        /// <returns>Singleton Object</returns>
        public static DataManager GetInstance()
        {
            return instance;
        }
    }
}
