﻿// -----------------------------------------------------------------------
// <copyright file="SalesPlanImportDAL.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 05/26/2016 01:01:58 AM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------
namespace DataExchangeApplication.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.DAO;
    using DataExchangeApplication.Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SalesPlanImportDAL : IDataImportDAL
    {
        /// <summary>
        /// Import SalesPlan from Head Office
        /// </summary>
        /// <param name="data">SalesPlan data from Head Office</param>
        /// <returns>database operation status</returns>
        public string ImportData(object data)
        {
            List<SalesPlanDAO> saleList = data as List<SalesPlanDAO>;
            DataManager manager = DataManager.GetInstance();

            foreach (SalesPlanDAO salePlan in saleList)
            {
                Entity.MonthlyProductionPlanInfo monthlyPlantInfo = new MonthlyProductionPlanInfo();
                monthlyPlantInfo.ID = salePlan.ID;
                monthlyPlantInfo.DateAdded = DateTime.Now;
                monthlyPlantInfo.IsDeleted = false;
                monthlyPlantInfo.LastModified = DateTime.Now;
                monthlyPlantInfo.PlanDate = salePlan.PlanDate;
                monthlyPlantInfo.Revision = salePlan.Revision;
                monthlyPlantInfo.PlanMonthYear = salePlan.PlanDate.Date;
                monthlyPlantInfo.MonthlyProductionPlanDetails = new System.Data.Objects.DataClasses.EntityCollection<MonthlyProductionPlanDetail>();
                foreach (SalesPlanDetailDAO salePlanDetail in salePlan.SPDetail)
                {
                    monthlyPlantInfo.MonthlyProductionPlanDetails.Add(new MonthlyProductionPlanDetail()
                        {
                            ProductID = salePlanDetail.ProductId,
                            DateAdded = DateTime.Now,
                            LastModified = DateTime.Now,
                            IsDeleted = false,
                            Remark = salePlanDetail.Remark
                        });
                }

                manager.FactoryContext.MonthlyProductionPlanInfoes.AddObject(monthlyPlantInfo);
            }

            manager.FactoryContext.SaveChanges();
            return Utility.ConstantStringCollection.IMPORTSUCCESS;
        }
    }
}
