﻿// -----------------------------------------------------------------------
// <copyright file="SalesPlanExportDAL.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 05/24/2016 12:00:00 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.DAO;
    using DataExchangeApplication.Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SalesPlanExportDAL : IDataExportDAL
    {
        /// <summary>
        /// Exporting data
        /// </summary>
        /// <returns>return exported data list</returns>
        public object ExportData()
        {
            List<SalesPlanDAO> saleplanRequestList = new List<SalesPlanDAO>();
            DataManager manager = DataManager.GetInstance();
            List<SalesPlan> headofficeRequestList = (from sp in manager.HeadOfficeContext.SalesPlans where sp.IsDeleted == false && sp.IsExported == false select sp).ToList();

            foreach (SalesPlan sp in headofficeRequestList)
            {
                saleplanRequestList.Add(new SalesPlanDAO(sp));
            }

            return saleplanRequestList;
        }

        /// <summary>
        /// change export status of the exported data
        /// </summary>
        public void ChangeExportStatus()
        {
        }
    }
}
