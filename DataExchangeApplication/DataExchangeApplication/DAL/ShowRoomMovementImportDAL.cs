﻿// -----------------------------------------------------------------------
// <copyright file="ShowRoomMovementImportDAL.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.DAO;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ShowRoomMovementImportDAL:IDataImportDAL
    {
        /// <summary>
        /// Import SalesPlan from Head Office
        /// </summary>
        /// <param name="data">SalesPlan data from Head Office</param>
        /// <returns>database operation status</returns>
        public string ImportData(object data)
        {
            List<ShowRoomMovementHeaderDAO> saleList = data as List<ShowRoomMovementHeaderDAO>;
            DataManager manager = DataManager.GetInstance();

            foreach (ShowRoomMovementHeaderDAO salePlan in saleList)
            {
                
            }

            manager.FactoryContext.SaveChanges();
            return Utility.ConstantStringCollection.IMPORTSUCCESS;
        }

    }
}
