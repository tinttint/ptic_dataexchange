﻿// -----------------------------------------------------------------------
// <copyright file="ExportDataFactory.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/21/2016 5:04:38 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAL
{
    using System;
    using DataExchangeApplication.Utility;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ExportDataFactory
    {
        /// <summary>
        /// Get Data Access Layer
        /// </summary>
        /// <param name="dalType">Type Name</param>
        /// <returns>Suitable Data Access Layer</returns>
        public static IDataExportDAL GetDAL(string dalType)
        {
            if (dalType == null)
            {
                return null;
            }
            else if (dalType.Equals(ConstantStringCollection.FINISHEDGOODSREQUEST, StringComparison.OrdinalIgnoreCase))
            {
                return new FinishedGoodRequestExportDAL();
            }
            else if (dalType.Equals(ConstantStringCollection.FINISHEDGOODSISSUE, StringComparison.OrdinalIgnoreCase))
            {
                return new FinishedGoodRequestExportDAL();
            }
            else if (dalType.Equals(ConstantStringCollection.SALESPLAN, StringComparison.OrdinalIgnoreCase))
            {
                return new SalesPlanExportDAL();
            }
            else if (dalType.Equals(ConstantStringCollection.SHOWROOMMOVEMENT, StringComparison.OrdinalIgnoreCase))
            {
                return new ShowRoomMovementExportDAL();
            }
            else if (dalType.Equals(ConstantStringCollection.SERVICELEDGER, StringComparison.OrdinalIgnoreCase)) 
            {
                return new ServiceBatteryExportDAL();
            }
            else if (dalType.Equals(ConstantStringCollection.SERVICEDN, StringComparison.OrdinalIgnoreCase))
            {
                return new ServiceDNDAL();
            }

            return null;
        }
    }
}
