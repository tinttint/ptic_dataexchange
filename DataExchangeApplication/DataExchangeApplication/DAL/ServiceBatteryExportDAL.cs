﻿// -----------------------------------------------------------------------
// <copyright file="ServiceLedgerDA.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.DAO;
    using DataExchangeApplication.Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ServiceBatteryExportDAL:IDataExportDAL
    {
        /// <summary>
        /// Exporting data
        /// </summary>
        /// <returns>return exported data list</returns>
        public object ExportData()
        {
            List<ServiceBatteryLedgerDAO> svcLedgerList = new List<ServiceBatteryLedgerDAO>();
            DataManager manager = DataManager.GetInstance();
            List<ServiceLedger> headofficeRequestList = (from sl in manager.HeadOfficeContext.ServiceLedgers where sl.IsDeleted == false && sl.IsExported == false && sl.IsSend==true select sl).ToList();

            foreach (ServiceLedger sp in headofficeRequestList)
            {
                svcLedgerList.Add(new ServiceBatteryLedgerDAO(sp));
            }

            return svcLedgerList;
        }
        /// <summary>
        /// change export status of the exported data
        /// </summary>
        public void ChangeExportStatus()
        {
        }

    }
}
