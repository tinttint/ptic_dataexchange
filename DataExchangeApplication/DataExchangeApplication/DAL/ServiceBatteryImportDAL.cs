﻿// -----------------------------------------------------------------------
// <copyright file="ServiceDNImportDAL.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
using DataExchangeApplication.DAO;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ServiceBatteryImportDAL:IDataImportDAL
    {

        /// <summary>
        /// Import SalesPlan from Head Office
        /// </summary>
        /// <param name="data">SalesPlan data from Head Office</param>
        /// <returns>database operation status</returns>
        public string ImportData(object data)
        {
            List<ServiceBatteryLedgerDAO> saleList = data as List<ServiceBatteryLedgerDAO>;
            DataManager manager = DataManager.GetInstance();

            foreach (ServiceBatteryLedgerDAO svcLedgerDAO in saleList)
            {

                //duplicate checking
                Entity.ServiceLedgerHeadOffice hoDuplicatet = (from hoSvc in manager.FactoryContext.ServiceLedgerHeadOffices
                                                               where svcLedgerDAO.JobNo == hoSvc.JobNo
                                                               && svcLedgerDAO.ProductId == hoSvc.ProductId
                                                               select hoSvc).FirstOrDefault();
                if (hoDuplicatet == null)
                {

                    manager.FactoryContext.ServiceLedgerHeadOffices.AddObject(new Entity.ServiceLedgerHeadOffice()
                    {
                        JobNo = svcLedgerDAO.JobNo,
                        Name = svcLedgerDAO.CustomerName,
                        ProductionDate = svcLedgerDAO.ProductionDate,
                        Duration = svcLedgerDAO.Duration,
                        ProductId = svcLedgerDAO.ProductId,
                        //RecieveDate = svcLedgerDAO.RecievedDate,
                        CaseCategoryId = svcLedgerDAO.CaseCategoryId,
                        FaultDetails = svcLedgerDAO.FaultsDetails,
                        CheckPoints = svcLedgerDAO.HOServiceRecords,
                        R1 = svcLedgerDAO.R1,
                        R2 = svcLedgerDAO.R2,
                        R3 = svcLedgerDAO.R3,
                        R4 = svcLedgerDAO.R4,
                        R5 = svcLedgerDAO.R5,
                        R6 = svcLedgerDAO.R6,
                        Volt = svcLedgerDAO.Volt,
                        RecieveDate = svcLedgerDAO.FactorySendDate,
                        
                        
                    });

                    manager.FactoryContext.SaveChanges();
                }
            }

           
            return Utility.ConstantStringCollection.IMPORTSUCCESS;
        }
    }
}
