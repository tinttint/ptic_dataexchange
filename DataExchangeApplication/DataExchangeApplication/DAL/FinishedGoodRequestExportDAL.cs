﻿// -----------------------------------------------------------------------
// <copyright file="FinishedGoodRequestExportDAL.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/23/2016 1:46:27 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAL
{
    using System.Collections.Generic;
    using System.Linq;
    using DataExchangeApplication.DAO;
    using Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class FinishedGoodRequestExportDAL : IDataExportDAL
    {
        /// <summary>
        /// Exporting Data 
        /// </summary>
        /// <returns>Exported Data List</returns>
        public object ExportData()
        {
            if (Properties.Settings.Default.CurrentLocation == Utility.ConstantStringCollection.HEADOFFICE)
            {
                List<FinishedGoodsRequisitionDAO> finishedGoodsRequestList = new List<FinishedGoodsRequisitionDAO>();
                DataManager manager = DataManager.GetInstance();
                List<FGRequest> headOfficeRequestList = (from fgList in manager.HeadOfficeContext.FGRequests where !fgList.IsDeleted && !fgList.IsExported select fgList).ToList();

                foreach (FGRequest request in headOfficeRequestList)
                {
                    finishedGoodsRequestList.Add(new FinishedGoodsRequisitionDAO(request));
                }

                return finishedGoodsRequestList;
            }
            else 
            {
                DataManager manager = DataManager.GetInstance();
                List<FGIssueNoteInfoDAO> finishedGoodsIsList = new List<FGIssueNoteInfoDAO>();
                List<FGIssueNoteInfo> infoList = (from fg in manager.FactoryContext.FGIssueNoteInfoes where !fg.IsExported && !fg.IsDeleted.Value  && fg.HoRequestNo != string.Empty select fg).ToList();

                foreach (FGIssueNoteInfo info in infoList)
                {
                    finishedGoodsIsList.Add(new FGIssueNoteInfoDAO(info));
                }

                return finishedGoodsIsList;
            }
        }

        /// <summary>
        /// Change Export Status of Exported Data
        /// </summary>
        public void ChangeExportStatus()
        {
        }
    }
}
