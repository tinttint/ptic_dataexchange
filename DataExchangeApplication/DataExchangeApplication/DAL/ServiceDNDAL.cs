﻿// -----------------------------------------------------------------------
// <copyright file="ServiceDNDAL.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.DAO;
    using DataExchangeApplication.Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ServiceDNDAL:IDataExportDAL
    {
        /// <summary>
        /// Exporting data
        /// </summary>
        /// <returns>return exported data list</returns>
        public object ExportData()
        {
            if (Properties.Settings.Default.CurrentLocation == Utility.ConstantStringCollection.HEADOFFICE)
            {
                List<ServiceDeliveryNoteInfoDAO> svcLedgerList = new List<ServiceDeliveryNoteInfoDAO>();
                DataManager manager = DataManager.GetInstance();
                List<ServiceBatteryDeliveryNoteInfo> headofficeRequestList = (from sl in manager.HeadOfficeContext.ServiceBatteryDeliveryNoteInfoes where sl.IsDeleted == false && sl.IsExported == false select sl).ToList();

                foreach (ServiceBatteryDeliveryNoteInfo sp in headofficeRequestList)
                {
                    svcLedgerList.Add(new ServiceDeliveryNoteInfoDAO(sp));
                }

                return svcLedgerList;
            }
            else 
            {
                List<ServiceDeliveryNoteInfoDAO> svcLedgerList = new List<ServiceDeliveryNoteInfoDAO>();
                DataManager manager = DataManager.GetInstance();
                List<ServiceIssuedNoteInfo> factoryIssuedList = (from sl in manager.FactoryContext.ServiceIssuedNoteInfoes where sl.IsDeleted == false && sl.IsExported == false select sl).ToList();

                foreach (ServiceIssuedNoteInfo sp in factoryIssuedList)
                {
                    svcLedgerList.Add(new ServiceDeliveryNoteInfoDAO(sp));
                }

                return svcLedgerList;
            }
        }
        /// <summary>
        /// change export status of the exported data
        /// </summary>
        public void ChangeExportStatus()
        {

        }
    }
}
