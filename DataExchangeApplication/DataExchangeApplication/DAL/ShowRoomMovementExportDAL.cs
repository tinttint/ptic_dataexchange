﻿// -----------------------------------------------------------------------
// <copyright file="ShowRoomMovementExportDAL.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 06/13/2016 2:31:27 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.DAO;
    using DataExchangeApplication.Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ShowRoomMovementExportDAL : IDataExportDAL
    {
        /// <summary>
        /// Exporting Data
        /// </summary>
        /// <returns>Exported data list</returns>
        public object ExportData()
        {
            DataManager manager = DataManager.GetInstance();
            List<ShowRoomMovementHeaderDAO> showRoomHeaderList = new List<ShowRoomMovementHeaderDAO>();
            List<WarehouseMovementHeader> wareHouseHeaderList = (from header in manager.HeadOfficeContext.WarehouseMovementHeaders 
                                                                 where header.IsDeleted == false && header.IsExported ==false 
                                                                 && header.MoveTo == 1
                                                                 select header).ToList();

            foreach (WarehouseMovementHeader header in wareHouseHeaderList)
            {
                showRoomHeaderList.Add(new ShowRoomMovementHeaderDAO(header));
            }

            return showRoomHeaderList;
        }

        /// <summary>
        /// Change Export status of Exported data
        /// </summary>
        public void ChangeExportStatus()
        {
        }
    }
}
