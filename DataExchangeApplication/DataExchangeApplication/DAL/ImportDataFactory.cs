﻿// -----------------------------------------------------------------------
// <copyright file="ImportDataFactory.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/21/2016 5:04:38 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAL
{
    using System;
    using DataExchangeApplication.Utility;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ImportDataFactory
    {
        /// <summary>
        /// Get Data Access Layer
        /// </summary>
        /// <param name="dalType">Type Name</param>
        /// <returns>Suitable Data Access Layer</returns>
        public static IDataImportDAL GetDAL(string dalType)
        {
            if (dalType == null)
            {
                return null;
            }
            else if (dalType.Equals(ConstantStringCollection.FINISHEDGOODSREQUEST, StringComparison.OrdinalIgnoreCase))
            {
                return new FinishedGoodRequestImportDAL();
            }
            else if (dalType.Equals(ConstantStringCollection.FINISHEDGOODSISSUE, StringComparison.OrdinalIgnoreCase))
            {
                return new FinishedGoodRequestImportDAL();
            }
            else if (dalType.Equals(ConstantStringCollection.SALESPLAN, StringComparison.OrdinalIgnoreCase))
            {
                return new SalesPlanImportDAL();
            }
            else if (dalType.Equals(ConstantStringCollection.SHOWROOMMOVEMENT, StringComparison.OrdinalIgnoreCase))
            {
                return new ShowRoomMovementImportDAL();
            }
            else if (dalType.Equals(ConstantStringCollection.SERVICELEDGER, StringComparison.OrdinalIgnoreCase))
            {
                return new ServiceBatteryImportDAL();
            }
            else if (dalType.Equals(ConstantStringCollection.SERVICEDN, StringComparison.OrdinalIgnoreCase))
            {
                return new ServiceDNImportDAL();
            }

            return null;
        }
    }
}
