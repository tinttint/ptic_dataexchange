﻿// -----------------------------------------------------------------------
// <copyright file="Login.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 06/15/2016 3:04:38 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;

    /// <summary>
    /// This is Login UI
    /// </summary>
    public partial class Login : Form
    {
        /// <summary>
        /// user name to check
        /// </summary>
        private string userName = "admin";

        /// <summary>
        /// password to check
        /// </summary>
        private string password = "provenfactory";

        /// <summary>
        /// Initializes a new instance of the Login class.
        /// </summary>
        public Login()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Check UserName and Password is valid or not
        /// </summary>
        private void LoginValidation()
        {
            string username = txtUserName.Text;
            string pwd = txtPassword.Text;
            if (username.Equals(this.userName) && pwd.Equals(this.password))
            {
                this.Visible = false;
                MainForm mainForm = new MainForm();
                mainForm.ShowDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("Invalid UserName & Password!");
                this.txtUserName.Text = null;
                this.txtPassword.Text = null;
                this.txtUserName.Focus();
                return;
            }
        }

        /// <summary>
        /// For login
        /// </summary>
        /// <param name="sender">sender as button</param>
        /// <param name="e">e as event</param>
        private void BtnLogin_Click_1(object sender, EventArgs e)
        {
            this.LoginValidation();
        }

        /// <summary>
        /// For exit
        /// </summary>
        /// <param name="sender">sender as button</param>
        /// <param name="e">e as event</param>
        private void BtnExit_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblUserLogin_Click(object sender, EventArgs e)
        {

        }
    }
}
