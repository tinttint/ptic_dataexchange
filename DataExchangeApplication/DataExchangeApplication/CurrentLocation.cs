﻿// -----------------------------------------------------------------------
// <copyright file="CurrentLocation.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/21/2016 5:04:38 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;

    /// <summary>
    /// Location Setting
    /// </summary>
    public partial class CurrentLocation : Form
    {
        /// <summary>
        /// Initializes a new instance of the CurrentLocation class.
        /// </summary>
        public CurrentLocation()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// On Load Method
        /// </summary>
        /// <param name="sender">GUI Form</param>
        /// <param name="e">Load Event</param>
        private void FrmCurrentLocation_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.CurrentLocation == Utility.ConstantStringCollection.HEADOFFICE)
            {
                this.rdoHeadOffice.Checked = true;
            }
            else 
            {
                this.rdoFactory.Checked = true;
            }
        }

        /// <summary>
        /// Close Function
        /// </summary>
        /// <param name="sender">Close Button</param>
        /// <param name="e">Click Event</param>
        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Save Function
        /// </summary>
        /// <param name="sender">Save Button</param>
        /// <param name="e">Click Event</param>
        private void Button1_Click(object sender, EventArgs e)
        {
            if (rdoHeadOffice.Checked)
            {
                Properties.Settings.Default.CurrentLocation = Utility.ConstantStringCollection.HEADOFFICE;
            }
            else 
            {
                Properties.Settings.Default.CurrentLocation = Utility.ConstantStringCollection.FACTORY;
            }

            Properties.Settings.Default.Save();
            MessageBox.Show("Success!");
            this.Close();
        }
    }
}
