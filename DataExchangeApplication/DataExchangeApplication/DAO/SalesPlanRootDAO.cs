﻿// -----------------------------------------------------------------------
// <copyright file="SalesPlanRootDAO.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 05/24/2016 3:24:41 AM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SalesPlanRootDAO
    {
        /// <summary>
        /// Gets or sets primary key column
        /// </summary>
        public List<SalesPlanDAO> SalesPlanRequisitionCollection { get; set; }
    }
}
