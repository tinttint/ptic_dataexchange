﻿// -----------------------------------------------------------------------
// <copyright file="ShowRoomMovementRootDAO.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 06/13/2016 2:07:15 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ShowRoomMovementRootDAO
    {
        /// <summary>
        /// Gets or sets Primary Key column
        /// </summary>
        public List<ShowRoomMovementHeaderDAO> ShowRoomMovementCollection { get; set; }
    }
}
