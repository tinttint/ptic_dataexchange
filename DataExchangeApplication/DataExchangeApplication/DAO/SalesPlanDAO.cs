﻿// -----------------------------------------------------------------------
// <copyright file="SalesPlanDAO.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 05/24/2016 3:24:41 AM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.Entity;

    /// <summary>
    /// SalesPlan Requisition Template
    /// </summary>
    public class SalesPlanDAO
    {
        #region constructor & copyconstructor
        /// <summary>
        /// Initializes a new instance of the SalesPlanDAO class.
        /// </summary>
        /// <param name="salePlan">Data from Head Office Context</param>
        public SalesPlanDAO(SalesPlan salePlan)
        {
            this.ID = salePlan.ID;
            this.PlanDate = salePlan.DateAdded;
            this.Revision = salePlan.Revision;
            this.SalePlanAmt = salePlan.SalesPlanAmt.Value;
            this.Status = salePlan.Status.Value;
            this.SPDetail = new List<SalesPlanDetailDAO>();
            var details = (from detail in salePlan.SalesPlanDetails where !detail.IsDeleted select detail).ToList();

            foreach (SalesPlanDetail d in details)
            {
                this.SPDetail.Add(new SalesPlanDetailDAO(d));
            }
        }

        /// <summary>
        /// Initializes a new instance of the SalesPlanDAO class.
        /// </summary>
        public SalesPlanDAO()
        {
        }

        #endregion

        #region properties
        /// <summary>
        /// Gets or sets SalesPlan ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets PlanDate
        /// </summary>
        public DateTime PlanDate { get; set; }

        /// <summary>
        /// Gets or sets DateAdded
        /// </summary>
        public DateTime DateAdded { get; set; }

        /// <summary>
        /// Gets or sets LastModified Date
        /// </summary>
        public DateTime LastModified { get; set; }

        /// <summary>
        /// Gets or sets SalePlanAmount
        /// </summary>
        public decimal SalePlanAmt { get; set; }

        /// <summary>
        /// Gets or sets Status value
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the item is deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets Revision value
        /// </summary>
        public string Revision { get; set; }

        /// <summary>
        /// Gets or sets SalesPlanRequisitionDetailDAO
        /// </summary>
        public List<SalesPlanDetailDAO> SPDetail { get; set; }
        #endregion
    }
}
