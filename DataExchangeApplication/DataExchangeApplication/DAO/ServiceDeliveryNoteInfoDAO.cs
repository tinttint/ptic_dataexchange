﻿// -----------------------------------------------------------------------
// <copyright file="ServiceDeliveryNoteInfoDAO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ServiceDeliveryNoteInfoDAO
    {

        public ServiceDeliveryNoteInfoDAO() { }

        public ServiceDeliveryNoteInfoDAO(ServiceBatteryDeliveryNoteInfo info) 
        {
            DataExchangeApplication.DAL.DataManager manager = DataExchangeApplication.DAL.DataManager.GetInstance();
            this.DeliveryNoteNo = info.DeliveryNoteNo;
            this.Issued = (from emp in manager.HeadOfficeContext.Employees where emp.ID == info.IssuedBy.Value select emp).FirstOrDefault().EmpName;
            this.Prepare = (from emp in manager.HeadOfficeContext.Employees where emp.ID == info.PrepareBy.Value select emp).FirstOrDefault().EmpName;
            this.SalesManager = (from emp in manager.HeadOfficeContext.Employees where emp.ID == info.SalesManager.Value select emp).FirstOrDefault().EmpName;
            
            this.IssuedDate = info.IssuedDate.Value;
            this.VenNo = (from v in manager.HeadOfficeContext.Vehicles where v.ID == info.VenId select v.VenNo).FirstOrDefault();
            this.Remark = info.Remark;
            this.ItemList = new List<ServiceDeliveryNoteDetailsDAO>();
            foreach (ServiceBatteryDeliveryNoteDetail dtl in info.ServiceBatteryDeliveryNoteDetails) 
            {
                this.ItemList.Add(new ServiceDeliveryNoteDetailsDAO(dtl));
            }
            
        }

        public ServiceDeliveryNoteInfoDAO(ServiceIssuedNoteInfo info)
        {
            DataExchangeApplication.DAL.DataManager manager = DataExchangeApplication.DAL.DataManager.GetInstance();
            Entity.vw_IssuedNoteInfo issueView =  ( from vw in manager.FactoryContext.vw_IssuedNoteInfo where vw.ID ==info.ID select vw).FirstOrDefault();

            this.DeliveryNoteNo = info.IssuedNo;
            this.Issued = issueView.Issuer;
            this.Prepare = issueView.Checker;
            //this.SalesManager = (from emp in manager.FactoryContext.Employees where emp.ID == info.SalesManager.Value select emp).FirstOrDefault().EmpName;
            this.IssuedDate = info.IssuedDate;
      
            this.VenNo = info.Venhicle;
            this.Remark = info.Remark;
            this.ItemList = new List<ServiceDeliveryNoteDetailsDAO>();
            foreach (ServiceIssuedNoteDetail dtl in info.ServiceIssuedNoteDetails)
            {
                this.ItemList.Add(new ServiceDeliveryNoteDetailsDAO(dtl));
            }

        }
        public string DeliveryNoteNo { get; set; }
        public string VenNo { get; set; }
        public string Prepare { get; set; }
        public string Issued { get; set; }
        public string SalesManager { get; set; }
        public DateTime IssuedDate { get; set; }
        public string Remark { get; set; }
     
        public List<ServiceDeliveryNoteDetailsDAO> ItemList { get; set; }

        public System.Data.Objects.DataClasses.EntityCollection<Entity.ServiceRecieveShowRoomDetail> GetDetailsEntities(Entity.Proven_FactoryEntities context) 
        {
            System.Data.Objects.DataClasses.EntityCollection<Entity.ServiceRecieveShowRoomDetail> dtlList = new System.Data.Objects.DataClasses.EntityCollection<ServiceRecieveShowRoomDetail>();
            foreach(ServiceDeliveryNoteDetailsDAO dtl in this.ItemList){
            dtlList.Add(new ServiceRecieveShowRoomDetail()
            {
                Duration = dtl.Duration,
                IsDeleted =false,
                JobNo = dtl.JobNo,
                ProductId =  (from svcLedger in context.ServiceLedgerHeadOffices where svcLedger.JobNo == dtl.JobNo select svcLedger.ProductId).FirstOrDefault(),
                ProdutionDate = dtl.ProductionDate
            });
            }
            return dtlList;
        }
    }
}
