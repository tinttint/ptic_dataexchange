﻿// -----------------------------------------------------------------------
// <copyright file="ServiceDeliveryNoteInfoDAO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ServiceDeliveryNoteDetailsDAO
    {
        public ServiceDeliveryNoteDetailsDAO() { }
        public ServiceDeliveryNoteDetailsDAO(Entity.ServiceBatteryDeliveryNoteDetail dtl) 
        {
            this.JobNo = dtl.JobNo;
            this.ProductName = dtl.ProductName;
            this.ProductionDate = dtl.ProductionDate;
            this.Remark = dtl.Remark;
            this.Duration = dtl.Duration;
            
        }

        public ServiceDeliveryNoteDetailsDAO(Entity.ServiceIssuedNoteDetail dtl)
        {
            DataExchangeApplication.DAL.DataManager manager = DataExchangeApplication.DAL.DataManager.GetInstance();
            this.JobNo = dtl.JobNo;
            this.ProductName = dtl.ProductName;
            Entity.ServiceLedgerHeadOffice hoSvcLedger = (from svcLedger in manager.FactoryContext.ServiceLedgerHeadOffices
                                                          where svcLedger.JobNo == dtl.JobNo
                                                          select svcLedger).FirstOrDefault();

            this.FactoryMangerRemark = hoSvcLedger.CheckPoints;
            this.Fault = (!hoSvcLedger.IsFactoryFault.HasValue? "":hoSvcLedger.IsFactoryFault.Value? "P" : "C");
            

        }

        
        public string JobNo { get; set; }
        public string ProductName { get; set; }
        public string Duration { get; set; }
        public string ProductionDate { get; set; }
        public string Remark { get; set; }
        public string FactoryMangerRemark { get; set; }
        public string Fault { get; set; }
    }
}