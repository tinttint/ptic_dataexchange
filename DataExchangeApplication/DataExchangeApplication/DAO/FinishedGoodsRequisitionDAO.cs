﻿// -----------------------------------------------------------------------
// <copyright file="FinishedGoodsRequisitionDAO.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/22/2016 11:28:00 AM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataExchangeApplication.DAL;
    using Entity;

    /// <summary>
    /// Finished Goods Requisition Template
    /// </summary>
    public class FinishedGoodsRequisitionDAO
    {
        /// <summary>
        /// Initializes a new instance of the FinishedGoodsRequisitionDAO class.
        /// </summary>
        /// <param name="requsetHo">Data from Head Office Context</param>
        public FinishedGoodsRequisitionDAO(FGRequest requsetHo)
        {
            DataManager manager = DataManager.GetInstance();
            this.ID = requsetHo.ID;
            this.ReqNo = requsetHo.ReqVouNo;
            this.ReqDate = requsetHo.ReqDate;
            this.Remark = requsetHo.Remark;
            this.Details = new List<FinishedGoodsRequisitionDetailsDAO>();
            Entity.Employee employee = (from emp in manager.HeadOfficeContext.Employees where emp.ID == requsetHo.RequesterID && emp.isDeleted == false select emp).FirstOrDefault();
            if (employee != null)
            {
                this.RequesterName = employee.EmpName;
            }

            var details = (from d in requsetHo.FGRequestDetails where !d.IsDeleted select d).ToList();

            foreach (FGRequestDetail detail in details)
            {
                FinishedGoodsRequisitionDetailsDAO finishedGoodsRequisition = new FinishedGoodsRequisitionDetailsDAO(detail);
                this.Details.Add(finishedGoodsRequisition);
            }
        }

        /// <summary>
        /// Initializes a new instance of the FinishedGoodsRequisitionDAO class.
        /// </summary>
        public FinishedGoodsRequisitionDAO()
        {
        }

        #region Properties

        /// <summary>
        /// Gets or sets Primary key column
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets Request Number
        /// </summary>
        public string ReqNo { get; set; }

        /// <summary>
        /// Gets or sets Issue Number
        /// </summary>
        public string IssueNo { get; set; }

        /// <summary>
        /// Gets or sets Issue Date
        /// </summary>
        public DateTime IssueDate { get; set; }

        /// <summary>
        /// Gets or sets Request Date
        /// </summary>
        public DateTime? ReqDate { get; set; }

        /// <summary>
        /// Gets or sets Require Date
        /// </summary>
        public DateTime RequireDate { get; set; }

        /// <summary>
        /// Gets or sets Van ID
        /// </summary>
        public int? TransportVenID { get; set; }

        /// <summary>
        /// Gets or sets Employee ID
        /// </summary>
        public int? TarnsportEmpID { get; set; }

        /// <summary>
        /// Gets or sets Request Employee ID
        /// </summary>
        public int RequesterID { get; set; }

        /// <summary>
        /// Gets or sets Request Employee Name
        /// </summary>
        public string RequesterName { get; set; }

        /// <summary>
        /// Gets or sets Remark
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// Gets or sets Factory Remark
        /// </summary>
        public string FactoryFormRemark { get; set; }

        /// <summary>
        /// Gets or sets Date Added
        /// </summary>
        public DateTime DateAdded { get; set; }

        /// <summary>
        /// Gets or sets Last Modified Date
        /// </summary>
        public DateTime LastModified { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the item is deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the item is exported.
        /// </summary>
        public bool IsExported { get; set; }

        /// <summary>
        /// Gets or sets Details
        /// </summary>
        public List<FinishedGoodsRequisitionDetailsDAO> Details { get; set; }
        #endregion
    }
}