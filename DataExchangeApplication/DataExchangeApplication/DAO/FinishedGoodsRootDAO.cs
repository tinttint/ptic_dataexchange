﻿// -----------------------------------------------------------------------
// <copyright file="FinishedGoodsRootDAO.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/23/2016 5:21:15 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System.Collections.Generic;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class FinishedGoodsRootDAO
    {
        /// <summary>
        /// Gets or sets Primary key column
        /// </summary>
        public List<FinishedGoodsRequisitionDAO> FinishedGoodsRequisitionCollection { get; set; }
    }
}
