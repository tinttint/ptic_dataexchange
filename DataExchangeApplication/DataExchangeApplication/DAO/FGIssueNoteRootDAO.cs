﻿// -----------------------------------------------------------------------
// <copyright file="FGIssueNoteRootDAO.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 05/31/2016 01:24:41 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class FGIssueNoteRootDAO
    {
        /// <summary>
        /// Gets or sets primary key column
        /// </summary>
        public List<FGIssueNoteInfoDAO> FinishedGoodsISCollection { get; set; }
    }
}
