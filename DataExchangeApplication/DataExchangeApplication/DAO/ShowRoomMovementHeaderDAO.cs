﻿// -----------------------------------------------------------------------
// <copyright file="ShowRoomMovementHeaderDAO.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 06/13/2016 1:27:51 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.DAL;
    using DataExchangeApplication.Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ShowRoomMovementHeaderDAO
    {
        #region Constructor & Copy Constructor
        /// <summary>
        /// Initializes a new instance of the ShowRoomMovementHeaderDAO class.
        /// </summary>
        /// <param name="wareHouseHeader">Data from Head Office Context</param>
        public ShowRoomMovementHeaderDAO(WarehouseMovementHeader wareHouseHeader)
        {
            DataManager manager = DataManager.GetInstance();
            this.ID = wareHouseHeader.ID;
            this.VenID = wareHouseHeader.VenID.HasValue ? wareHouseHeader.VenID.Value : 0;
            this.SalePersonID = wareHouseHeader.SalePersonID;
            this.MoveFrom = wareHouseHeader.MoveFrom;
            this.MoveFrom = wareHouseHeader.MoveFrom;
            this.MoveTo = wareHouseHeader.MoveTo;
            this.ReceivedDate = wareHouseHeader.ReceivedDate.HasValue ? wareHouseHeader.ReceivedDate.Value : DateTime.Now;
            this.DeliveredDate = wareHouseHeader.DeliveredDate.HasValue ? wareHouseHeader.DeliveredDate.Value : DateTime.Now;
            this.Status = wareHouseHeader.Status;
            this.Remark = wareHouseHeader.Remark;
            this.DateAdded = wareHouseHeader.DateAdded.HasValue ? wareHouseHeader.DateAdded.Value : DateTime.Now;
            this.LastModified = wareHouseHeader.LastModified.HasValue ? wareHouseHeader.LastModified.Value : DateTime.Now;
            this.IsDeleted = wareHouseHeader.IsDeleted ? wareHouseHeader.IsDeleted : false;

            this.Details = new List<ShowRoomMovementDetailDAO>();

            var details = (from dtl in wareHouseHeader.WarehouseMovementDetails where dtl.IsDeleted == false select dtl).ToList();

            foreach (WarehouseMovementDetail detail in manager.HeadOfficeContext.WarehouseMovementDetails)
            {
                ShowRoomMovementDetailDAO showRoomDetailDAO = new ShowRoomMovementDetailDAO(detail);
                this.Details.Add(showRoomDetailDAO);
            }
        }

        /// <summary>
        /// Initializes a new instance of the ShowRoomMovementHeaderDAO class.
        /// </summary>
        public ShowRoomMovementHeaderDAO()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets Primary Key column
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets foreign key column from Vehicle
        /// </summary>
        public int VenID { get; set; }

        /// <summary>
        /// Gets or sets foreign key column from Employee
        /// </summary>
        public int SalePersonID { get; set; }

        /// <summary>
        /// Gets or sets foreign key column from Warehouse
        /// </summary>
        public int MoveFrom { get; set; }

        /// <summary>
        /// Gets or sets foreign key column from Warehouse
        /// </summary>
        public int MoveTo { get; set; }

        /// <summary>
        /// Gets or sets delivered date
        /// </summary>
        public DateTime DeliveredDate { get; set; }

        /// <summary>
        /// Gets or sets Received date
        /// </summary>
        public DateTime ReceivedDate { get; set; }

        /// <summary>
        /// Gets or sets Added date
        /// </summary>
        public DateTime DateAdded { get; set; }

        /// <summary>
        /// Gets or sets last modified date
        /// </summary>
        public DateTime LastModified { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an item is deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an item is received
        /// </summary>
        public bool Status { get; set; }

        /// <summary>
        /// Gets or sets Remark
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// Gets or sets ShowRoomMovementDetailDAO
        /// </summary>
        public List<ShowRoomMovementDetailDAO> Details { get; set; }
        #endregion
    }
}
