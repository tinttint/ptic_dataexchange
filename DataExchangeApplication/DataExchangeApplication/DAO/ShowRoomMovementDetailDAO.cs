﻿// -----------------------------------------------------------------------
// <copyright file="ShowRoomMovementDetailDAO.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 06/13/2016 1:10:41 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ShowRoomMovementDetailDAO
    {
        #region Constructor & Copy Constructor
        /// <summary>
        /// Initializes a new instance of the ShowRoomMovementDetailDAO class.
        /// </summary>
        /// <param name="wareHouseDetail">Data from Head Office Context</param>
        public ShowRoomMovementDetailDAO(WarehouseMovementDetail wareHouseDetail)
        {
            this.ID = wareHouseDetail.ID;
            this.WarehouseMovementID = wareHouseDetail.WarehouseMovementID;
            this.ProductID = wareHouseDetail.ProductID;
            this.RequestQty = wareHouseDetail.RequestQty;
            this.ReceivedQty = wareHouseDetail.ReceivedQty.HasValue ? wareHouseDetail.ReceivedQty.Value : 0;
            this.DeliverQty = wareHouseDetail.DeliverQty;
            this.DateAdded = wareHouseDetail.DateAdded.HasValue ? wareHouseDetail.DateAdded.Value : DateTime.Now;
            this.LastModified = wareHouseDetail.LastModified.HasValue ? wareHouseDetail.LastModified.Value : DateTime.Now;
            this.IsDeleted = wareHouseDetail.IsDeleted ? wareHouseDetail.IsDeleted : false;
        }

        /// <summary>
        /// Initializes a new instance of the ShowRoomMovementDetailDAO class.
        /// </summary>
        public ShowRoomMovementDetailDAO()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets Primary Key
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets foreign key from WarehouseMovementHeader
        /// </summary>
        public int WarehouseMovementID { get; set; }

        /// <summary>
        /// Gets or sets foreign key from Product
        /// </summary>
        public int ProductID { get; set; }

        /// <summary>
        /// Gets or sets Request Quantity
        /// </summary>
        public decimal RequestQty { get; set; }

        /// <summary>
        /// Gets or sets Delivered Quantity
        /// </summary>
        public decimal DeliverQty { get; set; }

        /// <summary>
        /// Gets or sets Received Quantity
        /// </summary>
        public decimal ReceivedQty { get; set; }

        /// <summary>
        /// Gets or sets added date
        /// </summary>
        public DateTime DateAdded { get; set; }

        /// <summary>
        /// Gets or sets Last modified date
        /// </summary>
        public DateTime LastModified { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an item is deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        #endregion
    }
}
