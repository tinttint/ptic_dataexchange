﻿// -----------------------------------------------------------------------
// <copyright file="SalesPlanDetailDAO.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 05/24/2016 11:10:41 AM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SalesPlanDetailDAO
    {
        #region constructor & copyConstructor
        /// <summary>
        /// Initializes a new instance of the SalesPlanDetailDAO class.
        /// </summary>
        /// <param name="saleplanDetail">Detail data from Head Office</param>
        public SalesPlanDetailDAO(SalesPlanDetail saleplanDetail)
        {
            this.Id = saleplanDetail.ID;
            this.SalesPlanId = saleplanDetail.SalesPlanID;
            this.N100Convert = saleplanDetail.N100Convert.HasValue ? saleplanDetail.N100Convert.Value : 0;
            this.ProductId = saleplanDetail.ProductID;
            this.ProduceQty = saleplanDetail.ProduceQty.HasValue ? saleplanDetail.ProduceQty.Value : 0;
            this.RequireQty = saleplanDetail.RequireQty.HasValue ? saleplanDetail.RequireQty.Value : 0;
            this.RetailPrice = saleplanDetail.retailPrice.HasValue ? saleplanDetail.retailPrice.Value : 0;
            this.SaleQty = saleplanDetail.SaleQty.HasValue ? saleplanDetail.SaleQty.Value : 0;
            this.DateAdded = saleplanDetail.DateAdded;
            this.Remark = saleplanDetail.Remark;
        }

        /// <summary>
        /// Initializes a new instance of the SalesPlanDetailDAO class.
        /// </summary>
        public SalesPlanDetailDAO()
        {
        }

        #endregion

        #region properties

        /// <summary>
        /// Gets or sets ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets Product ID
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets SalesPlan ID
        /// </summary>
        public int SalesPlanId { get; set; }

        /// <summary>
        /// Gets or sets Retail Price
        /// </summary>
        public decimal RetailPrice { get; set; }

        /// <summary>
        /// Gets or sets Sales Quantity
        /// </summary>
        public decimal SaleQty { get; set; }

        /// <summary>
        /// Gets or sets Produce Quantity
        /// </summary>
        public decimal ProduceQty { get; set; }

        /// <summary>
        /// Gets or sets Required Quantity
        /// </summary>
        public decimal RequireQty { get; set; }

        /// <summary>
        /// Gets or sets N100 Convert value
        /// </summary>
        public double N100Convert { get; set; }

        /// <summary>
        /// Gets or sets Remark value
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// Gets or sets Added Date value
        /// </summary>
        public DateTime DateAdded { get; set; }

        /// <summary>
        /// Gets or sets Last Modified date value
        /// </summary>
        public DateTime LastModified { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the item is deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        #endregion
    }
}
