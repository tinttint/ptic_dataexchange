﻿// -----------------------------------------------------------------------
// <copyright file="ServiceBatteryLedgerDAO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ServiceBatteryLedgerDAO
    {
         #region constructor & copyconstructor
        /// <summary>
        /// Initializes a new instance of the SalesPlanDAO class.
        /// </summary>
        /// <param name="salePlan">Data from Head Office Context</param>
        public ServiceBatteryLedgerDAO(Entity.ServiceLedger sl)
        {
            this.ID = sl.Id;
            this.ProductId = sl.ProductId;
            this.RecievedDate = sl.RecieveDate;
            this.CaseCategoryId = (sl.CaseCategoryId.HasValue?sl.CaseCategoryId.Value:0);
            this.CustomerName = sl.Name;
            this.Duration = sl.Duration;
            this.FaultsDetails = sl.FaultDetails;
            this.GMRemark = sl.AGMRemark;
            this.HOServiceRecords = sl.HOServiceRecords;
            this.JobNo = sl.JobNo;
            this.ProductionDate = sl.ProductionDate;
            this.R1 = sl.R1;
            this.R2 = sl.R2;
            this.R3 = sl.R3;
            this.R4 = sl.R4;
            this.R5 = sl.R5;
            this.R6 = sl.R6;
            this.Substitute = sl.Substitute;
            this.Volt = sl.Volt;
            this.FactorySendDate = sl.FactorySendDate.HasValue? sl.FactorySendDate.Value:DateTime.Now.Date;

        }

        /// <summary>
        /// Initializes a new instance of the SalesPlanDAO class.
        /// </summary>
        public ServiceBatteryLedgerDAO()
        {
        }

        #endregion

        #region properties
        /// <summary>
        /// Gets or sets SalesPlan ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets PlanDate
        /// </summary>
        public DateTime RecievedDate { get; set; }

        /// <summary>
        /// Gets or sets DateAdded
        /// </summary>
        public string JobNo { get; set; }

        /// <summary>
        /// Gets or sets LastModified Date
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets SalePlanAmount
        /// </summary>
        public string  ProductionDate { get; set; }

        /// <summary>
        /// Gets or sets Status value
        /// </summary>
        public string  Duration { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the item is deleted.
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets Revision value
        /// </summary>
        public string R1 { get; set; }

        /// <summary>
        /// Gets or sets Revision value
        /// </summary>
        public string R2 { get; set; }
        /// <summary>
        /// Gets or sets Revision value
        /// </summary>
        public string R3 { get; set; }
        /// <summary>
        /// Gets or sets Revision value
        /// </summary>
        public string R4 { get; set; }
        /// <summary>
        /// Gets or sets Revision value
        /// </summary>
        public string R5 { get; set; }
        /// <summary>
        /// Gets or sets Revision value
        /// </summary>
        public string R6 { get; set; }

        public int CaseCategoryId { get; set; }

        public string Volt { get; set; }

        public string Substitute { get; set; }

        public string HOServiceRecords { get; set; }

        public string FaultsDetails { get; set; }

        public string GMRemark { get; set; }

        public DateTime FactorySendDate { get; set; }

        public DateTime FactoryRecieveDate {get;set;}


        #endregion


    }
}
