﻿// -----------------------------------------------------------------------
// <copyright file="ServieBatteryDeliveryNoteRootDAO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ServieBatteryDeliveryNoteRootDAO
    {

        /// <summary>
        /// Gets or sets primary key column
        /// </summary>
        public List<ServiceDeliveryNoteInfoDAO> DeliveryNoteCollections { get; set; }
    }
}
