﻿// -----------------------------------------------------------------------
// <copyright file="FGIssueNoteInfoDAO.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 05/31/2016 02:28:00 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
using DataExchangeApplication.Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class FGIssueNoteInfoDAO
    {
        #region constructor and copyconstructor
        /// <summary>
        /// Initializes a new instance of the FGIssueNoteInfoDAO class.
        /// </summary>
        /// <param name="finishedGoodsInfo">data from factory</param>
        public FGIssueNoteInfoDAO(FGIssueNoteInfo finishedGoodsInfo)
        {
            this.IssueNoteNo = finishedGoodsInfo.IssueNoteNo;
            this.IssueDate = finishedGoodsInfo.IssueDate.Value;
            this.HOReqNo = finishedGoodsInfo.HoRequestNo;
            this.Remark = finishedGoodsInfo.Remark;
            this.HORemark = finishedGoodsInfo.HORemark;

            this.Details = new List<FGIssueNoteDetailDAO>();
            var detail = (from dtl in finishedGoodsInfo.FGIssueNoteDetails where dtl.IsDeleted == false select dtl).ToList();
            foreach (FGIssueNoteDetail finishedGoodsDetail in detail)
            {
                FGIssueNoteDetailDAO finishedGoodsIsDtl = new FGIssueNoteDetailDAO(finishedGoodsDetail);
                this.Details.Add(finishedGoodsIsDtl);
            }
        }

        /// <summary>
        /// Initializes a new instance of the FGIssueNoteInfoDAO class.
        /// </summary>
        public FGIssueNoteInfoDAO()
        {
        }
        #endregion

        #region properties

        /// <summary>
        /// Gets or sets primary key data
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets foreign key data from FGRequisitionInfo table
        /// </summary>
        public int RequisitionID { get; set; }

        /// <summary>
        /// Gets or sets foreign key data from Customer table
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets foreign key data from Vehicle table
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// Gets or sets foreign key data from Department table
        /// </summary>
        public int IssueDeptID { get; set; }

        /// <summary>
        /// Gets or sets foreign key data from Department table
        /// </summary>
        public int ReceivedDeptID { get; set; }

        /// <summary>
        /// Gets or sets foreign key data from Employee table
        /// </summary>
        public int ReceivedEmpID { get; set; }

        /// <summary>
        /// Gets or sets foreign key data from Employee table
        /// </summary>
        public int IssueEmpID { get; set; }

        /// <summary>
        /// Gets or sets foreign key data from Employee table
        /// </summary>
        public int CheckedEmpID { get; set; }

        /// <summary>
        /// Gets or sets foreign key data from Vehicle table
        /// </summary>
        public string OtherVenNo { get; set; }

        /// <summary>
        /// Gets or sets IssueType data
        /// </summary>
        public string IssueType { get; set; }

        /// <summary>
        /// Gets or sets the waste reason
        /// </summary>
        public string WasteReason { get; set; }

        /// <summary>
        /// Gets or sets Other Reference No
        /// </summary>
        public string OtherRefNo { get; set; }

        /// <summary>
        /// Gets or sets received person
        /// </summary>
        public string ReceivedPerson { get; set; }

        /// <summary>
        /// Gets or sets Head Office Request No
        /// </summary>
        public string HOReqNo { get; set; }

        /// <summary>
        /// Gets or sets Issue Note No
        /// </summary>
        public string IssueNoteNo { get; set; }

        /// <summary>
        /// Gets or sets Remark data
        /// </summary>
        public string Remark { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether an item can delete or not
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the added date
        /// </summary>
        public DateTime DateAdded { get; set; }

        /// <summary>
        /// Gets or sets the last modified date
        /// </summary>
        public DateTime LastModified { get; set; }

        /// <summary>
        /// Gets or sets the issued date
        /// </summary>
        public DateTime IssueDate { get; set; }

        /// <summary>
        /// Gets or sets the issued date
        /// </summary>
        public string HORemark { get; set; }

        /// <summary>
        /// Gets or sets the FGIssueNoteDetailDAO
        /// </summary>
        public List<FGIssueNoteDetailDAO> Details { get; set; }

        #endregion
    }
}
