﻿// -----------------------------------------------------------------------
// <copyright file="FGIssueNoteDetailDAO.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 05/31/2016 03:20:00 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.DAL;
    using DataExchangeApplication.Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class FGIssueNoteDetailDAO
    {
        #region constructor and copyconstructor
        /// <summary>
        /// Initializes a new instance of the FGIssueNoteDetailDAO class.
        /// </summary>
        /// <param name="finishedGoodsIssueDetail">detail data from factory</param>
        public FGIssueNoteDetailDAO(FGIssueNoteDetail finishedGoodsIssueDetail)
        {
            this.ProdID = finishedGoodsIssueDetail.ProductID.HasValue ? finishedGoodsIssueDetail.ProductID.Value : 0;

            DataManager mgr = DataManager.GetInstance();
            Entity.RelatedProudct rp = (from relprod in mgr.FactoryContext.RelatedProudcts where relprod.RelatedProductID == this.ProdID select relprod).FirstOrDefault();
            if (rp != null) 
            {
                this.ProdID = rp.ProductID.Value;
            }

            this.Qty = finishedGoodsIssueDetail.Qty.HasValue ? finishedGoodsIssueDetail.Qty.Value : 0;
            this.Remark = finishedGoodsIssueDetail.Remark;
            this.HORemark = finishedGoodsIssueDetail.HORemark;
        }

        /// <summary>
        /// Initializes a new instance of the FGIssueNoteDetailDAO class.
        /// </summary>
        public FGIssueNoteDetailDAO()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// Gets or sets Primary Key
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets foreign key from FGIssueNoteInfo table
        /// </summary>
        public int FGIssueNoteInfoID { get; set; }

        /// <summary>
        /// Gets or sets foreign key from RequestDetail table
        /// </summary>
        public int ReqDtlID { get; set; }

        /// <summary>
        /// Gets or sets foreign key from Product table
        /// </summary>
        public int ProdID { get; set; }

        /// <summary>
        /// Gets or sets Quantity from table
        /// </summary>
        public decimal Qty { get; set; }

        /// <summary>
        /// Gets or sets Reference No from table
        /// </summary>
        public string RefNo { get; set; }

        /// <summary>
        /// Gets or sets Packing from table
        /// </summary>
        public string Packing { get; set; }

        /// <summary>
        /// Gets or sets Present AP Name from table
        /// </summary>
        public string PreAPName { get; set; }

        /// <summary>
        /// Gets or sets Remark from table
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// Gets or sets Head Office Remark from table
        /// </summary>
        public string HORemark { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether item is with present or not
        /// </summary>
        public bool IsPresent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether item can delete or not
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets added date to table
        /// </summary>
        public DateTime DateAdded { get; set; }

        /// <summary>
        /// Gets or sets last modified date
        /// </summary>
        public DateTime LastModified { get; set; }

        #endregion
    }
}
