﻿// -----------------------------------------------------------------------
// <copyright file="FinishedGoodsRequisitionDetailsDAO.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/23/2016 3:24:41 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.DAO
{
    using System;
    using Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class FinishedGoodsRequisitionDetailsDAO
    {
        #region Costructor & Copy Constructor
        /// <summary>
        /// Initializes a new instance of the FinishedGoodsRequisitionDetailsDAO class.
        /// </summary>
        /// <param name="headOfficeFinishedGoodsRequestDetail">Details Data From Head Office</param>
        public FinishedGoodsRequisitionDetailsDAO(FGRequestDetail headOfficeFinishedGoodsRequestDetail)
        {
            this.ID = headOfficeFinishedGoodsRequestDetail.ID;
            this.ProductID = headOfficeFinishedGoodsRequestDetail.ProductID.HasValue ? headOfficeFinishedGoodsRequestDetail.ProductID.Value : 0;
            this.RequsetQty = headOfficeFinishedGoodsRequestDetail.Qty.HasValue ? headOfficeFinishedGoodsRequestDetail.Qty.Value : 0;
            this.IssueQty = headOfficeFinishedGoodsRequestDetail.IssueQty.HasValue ? headOfficeFinishedGoodsRequestDetail.IssueQty.Value : 0;
            this.FGReqID = headOfficeFinishedGoodsRequestDetail.FGReqID.HasValue ? headOfficeFinishedGoodsRequestDetail.FGReqID.Value : 0;
            this.DateAdded = headOfficeFinishedGoodsRequestDetail.DateAdded;
            this.FactoryRemark = headOfficeFinishedGoodsRequestDetail.FactoryRemark;
            this.Remark = headOfficeFinishedGoodsRequestDetail.Remark;
            this.LastModified = headOfficeFinishedGoodsRequestDetail.LastModified.HasValue ? headOfficeFinishedGoodsRequestDetail.LastModified.Value : DateTime.Now;
            this.IsDeleted = headOfficeFinishedGoodsRequestDetail.IsDeleted;
        }

        /// <summary>
        /// Initializes a new instance of the FinishedGoodsRequisitionDetailsDAO class.
        /// </summary>
        public FinishedGoodsRequisitionDetailsDAO()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets Primary Key
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets Foreign Key of Finished Goods Requisition
        /// </summary>
        public int FGReqID { get; set; }

        /// <summary>
        /// Gets or sets Foreign Key of Product
        /// </summary>
        public int ProductID { get; set; }

        /// <summary>
        /// Gets or sets Issue Quantity
        /// </summary>
        public decimal IssueQty { get; set; }

        /// <summary>
        /// Gets or sets Request Quantity
        /// </summary>
        public decimal RequsetQty { get; set; }

        /// <summary>
        /// Gets or sets Remark from Factory
        /// </summary>
        public string FactoryRemark { get; set; }

        /// <summary>
        /// Gets or sets Remark
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// Gets or sets Created Date
        /// </summary>
        public DateTime DateAdded { get; set; }

        /// <summary>
        /// Gets or sets Last Modified Date
        /// </summary>
        public DateTime LastModified { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the item is deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        #endregion
    }
}
