﻿// -----------------------------------------------------------------------
// <copyright file="ShowRoomMovementImportBL.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.BLL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.DAO;
    using DataExchangeApplication.DAL;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ShowRoomMovementImportBL
    {
        /// <summary>
        /// Import SalesPlan Data from Head Office
        /// </summary>
        /// <param name="salePlanList">sales plan List from Head Office</param>
        /// <returns>database operation status</returns>
        public string ImportData(List<ShowRoomMovementHeaderDAO> salePlanList)
        {
            IDataImportDAL importer = ImportDataFactory.GetDAL(Utility.ConstantStringCollection.SHOWROOMMOVEMENT);
            importer.ImportData(salePlanList);
            return Utility.ConstantStringCollection.IMPORTSUCCESS;
        }

        /// <summary>
        /// Import SalesPlan Data from Head Office
        /// </summary>
        public void SetImported()
        {
            IDataImportDAL importer = ImportDataFactory.GetDAL(Utility.ConstantStringCollection.SHOWROOMMOVEMENT);
        }
    }
}
