﻿// -----------------------------------------------------------------------
// <copyright file="FinishedGoodsRequestImportBL.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 04/25/2016 11:10:10 AM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.BLL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.DAL;
    using DataExchangeApplication.DAO;
    
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class FinishedGoodsRequestImportBL
    {
        /// <summary>
        /// Import Finished Goods Data from Head Office
        /// </summary>
        /// <param name="daoList">Finished Goods Data from Head Office</param>
        /// <returns>database operation status</returns>
        public string ImportData(List<FinishedGoodsRequisitionDAO> daoList)
        {
                IDataImportDAL importer = ImportDataFactory.GetDAL(Utility.ConstantStringCollection.FINISHEDGOODSREQUEST);
                importer.ImportData(daoList);
                return Utility.ConstantStringCollection.IMPORTSUCCESS;
        }

        /// <summary>
        /// Import Finished Goods Data from Factory
        /// </summary>
        /// <param name="infoList">Finished Goods Data from Factory</param>
        /// <returns>database operation status</returns>
        public string ImportFactoryData(List<FGIssueNoteInfoDAO> infoList)
        {
            IDataImportDAL importer = ImportDataFactory.GetDAL(Utility.ConstantStringCollection.FINISHEDGOODSISSUE);
            importer.ImportData(infoList);
            return Utility.ConstantStringCollection.IMPORTSUCCESS;
        }

        /// <summary>
        /// Import finished goods data from Head Office
        /// </summary>
        public void SetImported()
        {
            IDataImportDAL importer = ImportDataFactory.GetDAL(Utility.ConstantStringCollection.FINISHEDGOODSREQUEST);
        }
    }
}
