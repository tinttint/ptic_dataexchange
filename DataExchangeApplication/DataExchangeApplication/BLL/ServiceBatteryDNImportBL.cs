﻿// -----------------------------------------------------------------------
// <copyright file="ShowRoomMovementImportBL.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.BLL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.DAO;
    using DataExchangeApplication.DAL;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ServiceBatteryDNImportBL
    {
        /// <summary>
        /// Import SalesPlan Data from Head Office
        /// </summary>
        /// <param name="salePlanList">sales plan List from Head Office</param>
        /// <returns>database operation status</returns>
        public string ImportData(List<ServiceDeliveryNoteInfoDAO> serviceDNList)
        {
            IDataImportDAL importer = ImportDataFactory.GetDAL(Utility.ConstantStringCollection.SERVICEDN);
            importer.ImportData(serviceDNList);
            return Utility.ConstantStringCollection.IMPORTSUCCESS;
        }

        /// <summary>
        /// Import SalesPlan Data from Head Office
        /// </summary>
        public void SetImported()
        {
            IDataImportDAL importer = ImportDataFactory.GetDAL(Utility.ConstantStringCollection.SERVICELEDGER);
        }
    }
}
