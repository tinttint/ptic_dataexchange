﻿// -----------------------------------------------------------------------
// <copyright file="SalesPlanImportBL.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 05/24/2016 1:30:41 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------
namespace DataExchangeApplication.BLL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataExchangeApplication.DAL;
    using DataExchangeApplication.DAO;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SalesPlanImportBL
    {
        /// <summary>
        /// Import SalesPlan Data from Head Office
        /// </summary>
        /// <param name="salePlanList">sales plan List from Head Office</param>
        /// <returns>database operation status</returns>
        public string ImportData(List<SalesPlanDAO> salePlanList)
        {
            IDataImportDAL importer = ImportDataFactory.GetDAL(Utility.ConstantStringCollection.SALESPLAN);
            importer.ImportData(salePlanList);
            return Utility.ConstantStringCollection.IMPORTSUCCESS;
        }

        /// <summary>
        /// Import SalesPlan Data from Head Office
        /// </summary>
        public void SetImported()
        {
            IDataImportDAL importer = ImportDataFactory.GetDAL(Utility.ConstantStringCollection.SALESPLAN);
        }
    }
}
