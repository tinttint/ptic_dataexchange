﻿// -----------------------------------------------------------------------
// <copyright file="FinishedGoodsRequestExportBL.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/23/2016 3:40:10 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.BLL
{
    using System.Collections.Generic;
    using DAL;
    using DAO;
    using Newtonsoft.Json;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class FinishedGoodsRequestExportBL
    {

        public static int DetailRecordCount { get; set; }


        /// <summary>
        /// Read newly requested Data from Head Office
        /// Convert them to JSON String
        /// Encrypt them
        /// </summary>
        /// <returns>Encrypted JSON String</returns>
        public static string GetExportString()
        {
            if (Properties.Settings.Default.CurrentLocation == Utility.ConstantStringCollection.HEADOFFICE)
            {
                string finalEncryptedJsonString = string.Empty;

                IDataExportDAL exporter = ExportDataFactory.GetDAL(Utility.ConstantStringCollection.FINISHEDGOODSREQUEST);
                List<FinishedGoodsRequisitionDAO> requestList = (List<FinishedGoodsRequisitionDAO>)exporter.ExportData();

                FinishedGoodsRootDAO finishedGoodsRoot = new FinishedGoodsRootDAO();
                finishedGoodsRoot.FinishedGoodsRequisitionCollection = requestList;

                DetailRecordCount = requestList.Count;

                string jsonString = JsonConvert.SerializeObject(finishedGoodsRoot);

                Utility.Security.SecurityManager mgr = Utility.Security.SecurityManager.GetInstance();
                finalEncryptedJsonString = mgr.EncryptJSON(jsonString);

                return finalEncryptedJsonString;
            }
            else
            {
                string finalEncryptJsonString = string.Empty;

                IDataExportDAL exporter = ExportDataFactory.GetDAL(Utility.ConstantStringCollection.FINISHEDGOODSISSUE);
                List<FGIssueNoteInfoDAO> issueList = (List<FGIssueNoteInfoDAO>)exporter.ExportData();

                FGIssueNoteRootDAO finishedGoodRoot = new FGIssueNoteRootDAO();
                finishedGoodRoot.FinishedGoodsISCollection = issueList;

                DetailRecordCount = issueList.Count;

                string jsonString = JsonConvert.SerializeObject(finishedGoodRoot);

                Utility.Security.SecurityManager manager = Utility.Security.SecurityManager.GetInstance();
                finalEncryptJsonString = manager.EncryptJSON(jsonString);

                return finalEncryptJsonString;
            }
        }

        /// <summary>
        /// Read newly requested Data from Head Office
        /// Convert them to JSON String
        /// Encrypt them
        /// </summary>
        public void SetExported()
        {
            IDataExportDAL exporter = ExportDataFactory.GetDAL(Utility.ConstantStringCollection.FINISHEDGOODSREQUEST);
        }
    }
}
