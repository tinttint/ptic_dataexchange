﻿// -----------------------------------------------------------------------
// <copyright file="SalesPlanExportBL.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : Tint Tint
// DateTime  : 05/24/2016 11:30:41 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.BLL
{
    using System.Collections.Generic;
    using DAL;
    using DAO;
    using Newtonsoft.Json;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SalesPlanExportBL
    {
        public static int DetailRecordCount { get; set; }

        /// <summary>
        /// Read newly requested Data from Head Office
        /// Convert them to JSON String
        /// Encrypt them
        /// </summary>
        /// <returns>Encrypted JSON String</returns>
        public static string GetExportString()
        {
            string finalEncryptedJsonString = string.Empty;

            IDataExportDAL exporter = ExportDataFactory.GetDAL(Utility.ConstantStringCollection.SALESPLAN);
            List<SalesPlanDAO> saleplanList = (List<SalesPlanDAO>)exporter.ExportData();

            SalesPlanRootDAO salesPlanRoot = new SalesPlanRootDAO();
            salesPlanRoot.SalesPlanRequisitionCollection = saleplanList;

            DetailRecordCount = saleplanList.Count;

            string jsonString = JsonConvert.SerializeObject(salesPlanRoot);

            Utility.Security.SecurityManager mgr = Utility.Security.SecurityManager.GetInstance();
            finalEncryptedJsonString = mgr.EncryptJSON(jsonString);

             return finalEncryptedJsonString;
        }

        /// <summary>
        /// Read newly requested Data from Head Office
        /// Convert them to JSON String
        /// Encrypt them
        /// </summary>
        public void SetExported()
        {
            IDataExportDAL exporter = ExportDataFactory.GetDAL(Utility.ConstantStringCollection.SALESPLAN);
        }
    }
}
