﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DataExchangeApplication
{
    public partial class ImportForm : Form
    {
        public delegate void CrossThreadInvoke(object obj);
        public delegate void CrossThreadInvoke2(object obj, object obj2);

        public ImportForm()
        {
            InitializeComponent();
        }

        private void ImportForm_Load(object sender, EventArgs e)
        {
            System.Threading.Thread exporterThread = new System.Threading.Thread(ImportingData);
            exporterThread.Start();
            dgvLog.Rows.Clear();   
        }

        private void ImportingData() 
        {
            try
            {
                if (Properties.Settings.Default.CurrentLocation == Utility.ConstantStringCollection.FACTORY)
                {

                    string directoryName = Properties.Settings.Default.ImportFilePath + "\\";
                    string searchPattern = string.Format("{1}_{2}.gz", Properties.Settings.Default.ExportFilePath, Utility.ConstantStringCollection.HEADOFFICE, DateTime.Now.ToString("yyyyMMdd") + "*");
                    string[] fileList = Directory.GetFiles(directoryName, searchPattern);
                    if (fileList.Length > 0)
                    {
                        foreach (string file in fileList)
                        {
                            SetProgress(6);
                            int index = file.LastIndexOf('\\');
                            string fileName = file.Substring(index + 1);
                            string newFileName = directoryName + "[Imported] " + fileName;
                            string encryptedJSON = Utility.GZip.GZipManager.DecompressStringFromFile(file);
                            Service.DataImportService serviceImporter = new Service.DataImportService();
                            serviceImporter.TaskCompleteProgress += new Service.DataImportService.CompleteProgess(serviceImporter_TaskCompleteProgress);
                            string finalImport = serviceImporter.ImportData(encryptedJSON);
                            System.IO.File.Move(file, newFileName);
                        }
                        MessageBox.Show(Utility.ConstantStringCollection.IMPORTSUCCESS);

                    }
                    else
                    {
                        MessageBox.Show("No File to Import!");
                        return;
                    }
                }
                else
                {

                    string directoryName = Properties.Settings.Default.ImportFilePath + "\\";
                    string searchPattern = string.Format("{1}_{2}.gz", Properties.Settings.Default.ExportFilePath, Utility.ConstantStringCollection.FACTORY, DateTime.Now.ToString("yyyyMMdd") + "*");
                    string[] fileList = Directory.GetFiles(directoryName, searchPattern);
                    if (fileList.Length > 0)
                    {
                        foreach (string file in fileList)
                        {
                            SetProgress(3);
                            int index = file.LastIndexOf('\\');
                            string fileName = file.Substring(index + 1);
                            string newFileName = directoryName + "[Imported] " + fileName;
                            string encryptedJSON = Utility.GZip.GZipManager.DecompressStringFromFile(file);
                            Service.DataImportService serviceImporter = new Service.DataImportService();
                            serviceImporter.TaskCompleteProgress += new Service.DataImportService.CompleteProgess(serviceImporter_TaskCompleteProgress);
                            string finalImport = serviceImporter.ImportData(encryptedJSON);
                            System.IO.File.Move(file, newFileName);
                        }
                        MessageBox.Show(Utility.ConstantStringCollection.IMPORTSUCCESS);

                    }
                    else
                    {
                        MessageBox.Show("No File to Import!");
                        return;
                    }
                }
            }
            catch (Exception err) 
            {
                MessageBox.Show(err.Message);
            }
        }


        private void btnExport_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void serviceImporter_TaskCompleteProgress(object sender, object e)
        {
            IncreaseProgress(this.pgTotalTask.Value + 1);
            SetMessage(sender);
            AddToLog(sender, e);
        }

        private void IncreaseProgress(object obj)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new CrossThreadInvoke(IncreaseProgress), obj);
            }
            else
            {
                this.pgTotalTask.Value = (int)obj;
            }

        }

        private void SetProgress(object obj)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new CrossThreadInvoke(SetProgress), obj);
            }
            else
            {
                this.pgTotalTask.Maximum = (int)obj;
                this.pgTotalTask.Value = 0;
            }
        }

        private void SetMessage(object obj)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new CrossThreadInvoke(SetMessage), obj);
            }
            else
            {
                this.lblMessage.Text = (string)obj;
            }
        }

        private void AddToLog(object obj, object obj2)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new CrossThreadInvoke2(AddToLog), obj, obj2);
            }
            else
            {
                dgvLog.Rows.Add();
                dgvLog.Rows[dgvLog.Rows.Count - 1].Cells[0].Value = obj;
                dgvLog.Rows[dgvLog.Rows.Count - 1].Cells[1].Value = obj2;
            }
        }
    }
}
