﻿// -----------------------------------------------------------------------
// <copyright file="MainForm.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/21/2016 5:04:38 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication
{
    using System;
    using System.IO;
    using System.Windows.Forms;
    using DataExchangeApplication.DAO;
    using DataExchangeApplication.Entity;

    /// <summary>
    /// This is Main GUI
    /// </summary>
    public partial class MainForm : Form
    {
        /// <summary>
        /// Initializes a new instance of the MainForm class.
        /// </summary>
        public MainForm()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Importing Data.
        /// </summary>
        /// <param name="sender">Import Data  Menu Item</param>
        /// <param name="e">Click Event</param>
        private void ImportDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportForm frmImport = new ImportForm();
            frmImport.ShowDialog();
        }

        /// <summary>
        /// Exporting Data.
        /// </summary>
        /// <param name="sender">Export Data Menu Item</param>
        /// <param name="e">Click Event</param>
        private void ExportDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExportForm frmExport = new ExportForm();
            frmExport.ShowDialog();
        }

        /// <summary>
        /// Administrative Setting of the Data Exchange Application.
        /// </summary>
        /// <param name="sender">Administrative Setting Menu Item</param>
        /// <param name="e">Click Event</param>
        private void AdministrativeSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
           ////throw new UnauthorizedAccessException();
        }

        /// <summary>
        /// To Change the Location Setting
        /// </summary>
        /// <param name="sender">ChangeLocationToolStripMenuItem data</param>
        /// <param name="e">Click Event</param>
        private void ChangeLocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurrentLocation locFrm = new CurrentLocation();
            locFrm.ShowDialog();
        }
    }
}