﻿// -----------------------------------------------------------------------
// <copyright file="DataExportService.cs" company="Ahtar Oo Company Ltd">
// TODO: Update copyright text.
// Developer : YanNaing
// DateTime  : 04/25/2016 12:21:22 PM
// copyright@2016
// </copyright>
// -----------------------------------------------------------------------

namespace DataExchangeApplication.Service
{
    using System.Collections.Generic;
    using System.Linq;
    using DataExchangeApplication.DAL;
    using DataExchangeApplication.Entity;
    using Newtonsoft.Json;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class DataExportService
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void CompleteProgess(object sender, object e);


        /// <summary>
        /// 
        /// </summary>
        public event CompleteProgess TaskCompleteProgress;

        

        /// <summary>
        /// update the entity after exported data
        /// </summary>
        public static void UpdateHOEntity()
        {
            DataManager manager = DataManager.GetInstance();
            var salePlanObject = from sp in manager.HeadOfficeContext.SalesPlans where sp.IsExported == false select sp;
            foreach (SalesPlan salePlan in salePlanObject)
            {
                salePlan.IsExported = true;
                salePlan.ExportedTime = System.DateTime.Now;
            }

            var finishedGoodsObject = from fg in manager.HeadOfficeContext.FGRequests where fg.IsExported == false select fg;
            foreach (FGRequest finishedGoodsRequest in finishedGoodsObject)
            {
                finishedGoodsRequest.IsExported = true;
                finishedGoodsRequest.ExportedTime = System.DateTime.Now;

            }

            var warehouseMovementObject = from warehouse in manager.HeadOfficeContext.WarehouseMovementHeaders where warehouse.IsExported == false && warehouse.MoveTo == 1 select warehouse;
            foreach (WarehouseMovementHeader header in warehouseMovementObject)
            {
                header.IsExported = true;
                header.ExportedTime = System.DateTime.Now;
            }

            var serviceLedgerObject = from svcLedger in manager.HeadOfficeContext.ServiceLedgers where svcLedger.IsExported == false && svcLedger.IsSend==true select svcLedger;
            foreach (ServiceLedger header in serviceLedgerObject)
            {
                header.IsExported = true;
                header.ExportedTime = System.DateTime.Now;
                
            }

            var serviceDNObject = from svcLedger in manager.HeadOfficeContext.ServiceBatteryDeliveryNoteInfoes where svcLedger.IsExported == false select svcLedger;
            foreach (ServiceBatteryDeliveryNoteInfo header in serviceDNObject)
            {
                header.IsExported = true;
                header.ExportedTime = System.DateTime.Now;
            }


            manager.HeadOfficeContext.SaveChanges();
        }

        /// <summary>
        /// Update the entity after exported data
        /// </summary>
        public static void UpdateFactoryEntity()
        {
            DataManager manager = DataManager.GetInstance();
            var finishedGoodsIssueObj = from Is in manager.FactoryContext.FGIssueNoteInfoes where Is.IsExported == false select Is;
            foreach (FGIssueNoteInfo info in finishedGoodsIssueObj)
            {
                info.IsExported = true;
                info.ExportedTime = System.DateTime.Now;
            }

            manager.FactoryContext.SaveChanges();
        }

        /// <summary>
        /// Export All Data From Head Office
        /// </summary>
        /// <returns>Encrypted Data JSON String</returns>
        public string ExportHeadOfficeData()
        {
            DataExchangeItemTemplate finishedGoodsRequestion, salesPlanRequisition, 
                showRoomMovement,serviceLedger, serviceDeliveryNote;
            string rootJSONString = string.Empty;

            DataExchangeTemplateRoot rootTemplate = new DataExchangeTemplateRoot();
            rootTemplate.CreatedDate = System.DateTime.Now;
            rootTemplate.CreatedLocation = "Head Office";
            rootTemplate.ItemList = new System.Collections.Generic.List<DataExchangeItemTemplate>();

            //// Finished Goods Requesition
            finishedGoodsRequestion = new DataExchangeItemTemplate();
            finishedGoodsRequestion.DataName = Utility.ConstantStringCollection.FINISHEDGOODSREQUEST;
            finishedGoodsRequestion.EncryptedJSON = BLL.FinishedGoodsRequestExportBL.GetExportString();

            rootTemplate.ItemList.Add(finishedGoodsRequestion);
                
            if (TaskCompleteProgress != null) {
                string msg = Utility.ConstantStringCollection.FINISHEDGOODSREQUEST;
                TaskCompleteProgress(msg,BLL.FinishedGoodsRequestExportBL.DetailRecordCount);
            }

            
            ////ShowRoomMovement
            showRoomMovement = new DataExchangeItemTemplate();
            showRoomMovement.DataName = Utility.ConstantStringCollection.SHOWROOMMOVEMENT;
            showRoomMovement.EncryptedJSON = BLL.ShowRoomMovementExportBL.GetExportedString();

            rootTemplate.ItemList.Add(showRoomMovement);

            if (TaskCompleteProgress != null) {
                string msg = Utility.ConstantStringCollection.SHOWROOMMOVEMENT;
                TaskCompleteProgress(msg , BLL.ShowRoomMovementExportBL.DetailRecordCount);
            }

            ////SalesPlan Requisiton
            salesPlanRequisition = new DataExchangeItemTemplate();
            salesPlanRequisition.DataName = Utility.ConstantStringCollection.SALESPLAN;
            salesPlanRequisition.EncryptedJSON = BLL.SalesPlanExportBL.GetExportString();

            rootTemplate.ItemList.Add(salesPlanRequisition);

            if (TaskCompleteProgress != null) {
                string msg = Utility.ConstantStringCollection.SALESPLAN;
                TaskCompleteProgress(msg , BLL.SalesPlanExportBL.DetailRecordCount);
            }

            ////serviceLedger Requisiton
            serviceLedger = new DataExchangeItemTemplate();
            serviceLedger.DataName = Utility.ConstantStringCollection.SERVICELEDGER;
            serviceLedger.EncryptedJSON = BLL.ServiceBatteryLedgerExportBL.GetExportString();

            rootTemplate.ItemList.Add(serviceLedger);

            if (TaskCompleteProgress != null) {
                string msg = Utility.ConstantStringCollection.SERVICELEDGER;
                TaskCompleteProgress(msg , BLL.ServiceBatteryLedgerExportBL.DetailRecordCount);
            }

            ////service delivery Note
            serviceDeliveryNote = new DataExchangeItemTemplate();
            serviceDeliveryNote.DataName = Utility.ConstantStringCollection.SERVICEDN;
            serviceDeliveryNote.EncryptedJSON = BLL.ServiceBatteryDNExportBL.GetExportString();

            rootTemplate.ItemList.Add(serviceDeliveryNote);
            if (TaskCompleteProgress != null)
            {
                string msg = Utility.ConstantStringCollection.SERVICEDN;
                TaskCompleteProgress(msg, BLL.ServiceBatteryDNExportBL.DetailRecordCount);
            }

            rootJSONString  = JsonConvert.SerializeObject(rootTemplate);

            Utility.Security.SecurityManager manager = Utility.Security.SecurityManager.GetInstance();

            return manager.EncryptJSON(rootJSONString);
        }

        /// <summary>
        /// Export All data from Factory
        /// </summary>
        /// <returns>Encrypted data JSON string</returns>
        public string ExportFactoryData()
        {
            DataExchangeItemTemplate finishedGoodsIssue;

            DataExchangeTemplateRoot tempRoot = new DataExchangeTemplateRoot();
            tempRoot.CreatedDate = System.DateTime.Now;
            tempRoot.CreatedLocation = "Factory";
            tempRoot.ItemList = new System.Collections.Generic.List<DataExchangeItemTemplate>();

            ////finishedgoods isssue
            finishedGoodsIssue = new DataExchangeItemTemplate();
            finishedGoodsIssue.DataName = Utility.ConstantStringCollection.FINISHEDGOODSISSUE;
            finishedGoodsIssue.EncryptedJSON = BLL.FinishedGoodsRequestExportBL.GetExportString();

            if (TaskCompleteProgress != null)
            {
                string msg = Utility.ConstantStringCollection.FINISHEDGOODSREQUEST;
                TaskCompleteProgress(msg, BLL.FinishedGoodsRequestExportBL.DetailRecordCount);
            }

            tempRoot.ItemList.Add(finishedGoodsIssue);

            string rootJSONString = string.Empty;
            rootJSONString = JsonConvert.SerializeObject(tempRoot);
            Utility.Security.SecurityManager manager = Utility.Security.SecurityManager.GetInstance();
            return manager.EncryptJSON(rootJSONString);
        }
    }
}
